<div class="panel panel-primary">
<div class="panel-body">
<div class="well well-sm">
  <ol class="messages">
    <h3>Tahapan Langkah Algoritma K-NN</h3>
    <li>Menentukan parameter k (jumlah tetangga paling dekat).</li>
    <li>Menghitung kuadrat jarak eucliden objek terhadap data training yang diberikan.</li>
    <li>Mengurutkan hasil no 2 secara ascending (berurutan dari nilai tinggi ke rendah)</li>
    <li>Mengumpulkan kategori Y (Klasifikasi nearest neighbor berdasarkan nilai k)</li>
    <li>Dengan menggunakan kategori nearest neighbor yang paling mayoritas maka dapat dipredisikan kategori objek.</li>
  </ol>
</div>
</div>
</div>
