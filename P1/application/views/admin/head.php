<style type="text/css">
.navbar-default .navbar-nav>li>a:hover, .navbar-default .navbar-nav>li>a:focus {
    color: #333;
    background-color: #455a64;
}

.navbar-default .navbar-nav>.open>a, .navbar-default .navbar-nav>.open>a:hover, .navbar-default .navbar-nav>.open>a:focus {
    background-color: #455a64;
    color: #333;
}
</style>
<div class="navbar navbar-default navbar-fixed-top header-highlight" style="    background-color: #263238;">
  <div class="navbar-header ">
    <a class="navbar-brand" href="<?php echo base_url(); ?>"><b style="color:white;font-size:17px;"><i>ADMIN</i></b></a>
    <ul class="nav navbar-nav visible-xs-block">
      <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
      <li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
    </ul>
  </div>

  <div class="navbar-collapse collapse " id="navbar-mobile">
    <ul class="nav navbar-nav">
      <li><a class="sidebar-control sidebar-main-toggle hidden-xs" style="    color: #f5f5f5;"><i class="icon-paragraph-justify3"></i></a></li>
      </ul>
    <ul class="nav navbar-nav navbar-right" >
      <li class="dropdown dropdown-user" >
        <a class="dropdown-toggle" data-toggle="dropdown" style="    color: #f5f5f5; ">
          <img src="" alt="">
            <!--<?php if($this->session->userdata('level')==1){echo "Superadmin";}else{echo "Admin";} ?>-->
            <?php echo $this->session->userdata('user') ?>
          <i class="caret"></i>
        </a>

        <ul class="dropdown-menu dropdown-menu-right" style="    color: #f5f5f5;">
           <?php 
              $user=$this->session->userdata('level');
              $username = $this->session->userdata('user');
              if ($user == 3){
                ?>
                  <li><a href="<?php echo site_url('peserta/Account/getUser/'.$username); ?>"><i class="fa fa-user"></i> Ubah Account</a></li>
                  <li><a href="<?php echo site_url('Dash/logout'); ?>" onclick="return confirm('Apakah Anda Yakin Ingin Keluar');"><i class="icon-switch2"></i> Logout</a></li>      
                <?php
              }else{?>
                <li><a href="" data-toggle="modal" data-target="#ganti" hidden="true"><i class="icon-cog5"></i>Ubah Username Dan Password</a></li>
                <li><a href="<?php echo site_url('Dash/logout'); ?>" onclick="return confirm('Apakah Anda Yakin Ingin Keluar');"><i class="icon-switch2"></i> Logout</a></li>
                <?php
              }
          ?>
          
        </ul>
      </li>
    </ul>
  </div>
</div>