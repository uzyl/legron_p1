<div class="box">
  <div><?php echo $this->session->flashdata('message'); ?></div>
  <!-- /.box-header -->
  <div class="panel panel-primary">
    <div class="panel-heading" style="    color: #fff;
      background-color: #2ECCFA;
      border-color: #2ECCFA;">
      <h5 class="panel-title"><i class="icon-collaboration"></i> Tabel Hasil</h5>
  </div>
  <!-- Panel Tabel -->
  <div class="panel-body">
  <div class="well well-sm">
  <div class="box-body" style="padding-left:0px; padding-top:0px; ">
    <!-- <div>
      <button type="button" class="btn btn-warning btn-sm" onclick="location.href='<?php echo base_url().'laporan'?>'"><i class="icon-file-plus"></i> Laporan </button>
    </div><br> -->
      <table id="example" class="table table-striped table-bordered" >
        <thead>
        <tr>
          <th>No.</th>
          <th>Nama</th>
          <th>Nilai UNBK</th>
          <th>Minat Siswa</th>
          <th>Saran BK</th>
        </tr>
        </thead>

          <?php $no=1; foreach ($getdata as $get){?>

        <tr>
          <td><?php echo $no; ?></td>
          <td><?php echo $get->nama_p; ?></td>
          <td><?php $string=$get->nilai_unbk_p; $unbk = preg_replace('/[^A-Za-z\-]/', '', $string) ;echo $unbk; ?></td>
          <td><?php $string=$get->minat_siswa_p; $minat = preg_replace('/[^A-Za-z\-]/', '', $string) ;echo $minat; ?></td>
          <td><?php $string=$get->saran_bk_p; $saran = preg_replace('/[^A-Za-z\-]/', '', $string) ;echo $saran; ?></td>
        </tr>
        <?php $no++; }?>
      </table>
    </div>
  </div>

<!-- P1 -->
<script type="text/javascript">
    $('#example').dataTable({
       "paging": true,
       "searching": true,
        "scrollX": true,
          dom: 'Bfrtip',
          lengthMenu: [
            [ 10, 25, 50, -1 ],
            [ '10 rows', '25 rows', '50 rows', 'Show all' ]
        ],
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });
</script>
