  <div class="panel panel-primary">
    <div class="panel-heading" style="color: #fff;
      background-color: #2ECCFA;
      border-color: #2ECCFA;">
      <h5 class="panel-title"><i class="icon-collaboration"></i> Edit Data Siswa</h5>
    </div>
<div class="well well-sm">
  <form action="<?php echo site_url('datasiswa/update_datasiswa'); ?>" method="post">
  <div class="row">
    <div class="col-md-6">
      <input type="hidden" name="id_datasiswa" id="id_datasiswa" required value="<?php echo $datasiswa[0]['id_datasiswa'] ?>">
      <center>
        <div class="form-group">
          <input type="text" name="nama" id="nama" autocomplete="off" placeholder="Nama" class="form-control" required value="<?php echo $datasiswa[0]['nama'] ?>">
        </div>
      </center>
    </div>
    <div class="col-md-6">
      <center>
        <div class="form-group">
          <?php foreach ($datasiswa as $data): ?>
         <select name="nilai_unbk" id="nilai_unbk" class="form-control" required value="<?php echo $datasiswa[0]['nilai_unbk']?>">
           <?php endforeach; ?>
            <!-- <optgroup label="Nilai UNBK"> -->
              <option value="">-- <?php $string=$datasiswa[0]['nilai_unbk']; $unbk = preg_replace('/[^A-Za-z\-]/', '', $string) ;echo $unbk;?> --</option>
              <option value="1_Tinggi">Tinggi</option>
              <option value="0.5_Sedang">Sedang</option>
              <option value="0_Rendah">Rendah</option>
            <!-- </optgroup> -->
        </select>
      </div>
      </center>
    </div>
    <div class="col-md-6">
      <center>
        <div class="form-group">
         <select name="minat_siswa" id="minat_siswa" class="form-control" required value="<?php echo $datasiswa[0]['minat_siswa']; ?>">
           <!-- <optgroup label="Minat Siswa"> -->
             <option value="">-- <?php $string=$datasiswa[0]['minat_siswa']; $minat = preg_replace('/[^A-Za-z\-]/', '', $string) ;echo $minat;?> --</option>
             <option value="1_SMK">SMK</option>
             <option value="0.5_SMA">SMA</option>
             <option value="0_MA">MA</option>
           <!-- </optgroup> -->
        </select>
      </div>
      </center>
    </div>
    <div class="col-md-6">
      <center>
        <div class="form-group">
         <select name="saran_bk" id="saran_bk" class="form-control" required value="<?php echo $datasiswa[0]['saran_bk'] ?>">
           <!-- <optgroup label="Saran BK"> -->
             <option value="">-- <?php $string=$datasiswa[0]['saran_bk']; $saran = preg_replace('/[^A-Za-z\-]/', '', $string) ;echo $saran;?> --</option>
             <option value="1_SMK">SMK</option>
             <option value="0.5_SMA">SMA</option>
             <option value="0_MA">MA</option>
           <!-- </optgroup> -->
        </select>
      </div>
      </center>
    </div>
    <div class="col-md-6">
      <center>
        <div class="form-group">
          <input type="text" name="melanjutkan_ke" id="me" autocomplete="off" placeholder="Melanjutkan Ke" class="form-control" required value="<?php echo $datasiswa[0]['melanjutkan_ke'] ?>">
        </div>
      </center>
    </div>
    <div class="col-md-6">
      <center>
        <div class="form-group">
          <input type="text" name="daerah" id="nama" autocomplete="off" placeholder="Daeerah" class="form-control" required value="<?php echo $datasiswa[0]['daerah'] ?>">
        </div>
      </center>
    </div>
  </div>
  <br>
  <div class="row">
      <center>
        <!-- <input id="insert_datasiswa" value="Tambah" type="submit" class="btn btn-warning btn-sm"><i class="icon-file-plus"></i>> -->
        <button type="submit" class="btn btn-info"><i class="fa fa-edit text-white"></i> Update </button>
      </center>
  </div>
</form>
</div>
