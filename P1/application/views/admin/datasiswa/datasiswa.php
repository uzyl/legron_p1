<!-- Notifikasi -->
<?php
$data=$this->session->flashdata('sukses');
if($data!=""){ ?>
<div class="alert alert-success"><strong>Sukses! </strong> <?=$data;?></div>
<?php } ?>
<?php
$data2=$this->session->flashdata('error');
if($data2!=""){ ?>
<div class="alert alert-danger"><strong> Error! </strong> <?=$data2;?></div>
<?php } ?>

<!-- panel -->
<div class="panel panel-primary">
  <div class="panel-heading" style="color:#fff; background-color:#2ECCFA; border-color: #2ECCFA;">
    <h5 class="panel-title"><i class="icon-collaboration"></i> Form Input Data Siswa</h5>
  </div>

  <!-- Panel Form -->
  <div class="panel-body">
  <div class="well well-sm">
    <form action="<?php echo site_url('datasiswa/insert_datasiswa'); ?>" method="post">
    <div class="row">
      <div class="col-md-6">
        <!-- <input type="hidden" name="id_datasiswa" id="id_datasiswa" required> -->
        <center>
          <div class="form-group">
            <input type="text" name="nama" id="nama" autocomplete="off" placeholder="Nama" class="form-control" required>
          </div>
        </center>
      </div>
      <div class="col-md-6">
        <center>
          <div class="form-group">
           <select name="nilai_unbk" id="nilai_unbk" class="select-clear" required>
              <!-- <optgroup label="Nilai UNBK"> -->
                <option value="">-- Nilai UNBK --</option>
                <option value="1_Tinggi">Tinggi</option>
                <option value="0.5_Sedang">Sedang</option>
                <option value="0_Rendah">Rendah</option>
              <!-- </optgroup> -->
          </select>
        </div>
        </center>
      </div>
      <div class="col-md-6">
        <center>
          <div class="form-group">
           <select name="minat_siswa" id="minat_siswa" class="select-clear" required>
             <!-- <optgroup label="Minat Siswa"> -->
               <option value="">-- Minat Siswa --</option>
               <option value="1_SMK">SMK</option>
               <option value="0.5_SMA">SMA</option>
               <option value="0_MA">MA</option>
             <!-- </optgroup> -->
          </select>
        </div>
        </center>
      </div>
      <div class="col-md-6">
        <center>
          <div class="form-group">
           <select name="saran_bk" id="saran_bk" class="select-clear" required>
             <!-- <optgroup label="Saran BK"> -->
               <option value="">-- Saran BK --</option>
               <option value="1_SMK">SMK</option>
               <option value="0.5_SMA">SMA</option>
               <option value="0_MA">MA</option>
             <!-- </optgroup> -->
          </select>
        </div>
        </center>
      </div>
      <div class="col-md-6">
        <center>
          <div class="form-group">
            <input type="text" name="melanjutkan_ke" id="melanjutkan_ke" autocomplete="off" placeholder="Melanjutkan Ke" class="form-control" required>
          </div>
        </center>
      </div>
      <div class="col-md-6">
        <center>
          <div class="form-group">
            <input type="text" name="daerah" id="daerah" autocomplete="off" placeholder="Daeerah" class="form-control" required>
          </div>
        </center>
      </div>
    </div>
    <br>
    <div class="row">
        <center>
          <!-- <input id="insert_datasiswa" value="Tambah" type="submit" class="btn btn-warning btn-sm"><i class="icon-file-plus"></i>> -->
          <button type="submit" id="insert_datasiswa" class="btn btn-info btn-sm"><i class="icon-file-plus"></i> Tambah </button>
        </center>
    </div>
  </form>
  </div>
</div><!--panel body-->

<div class="box">
  <div><?php echo $this->session->flashdata('message'); ?></div>
  <!-- /.box-header -->
  <div class="panel panel-primary">
    <div class="panel-heading" style="    color: #fff;
      background-color: #2ECCFA;
      border-color: #2ECCFA;">
      <h5 class="panel-title"><i class="icon-collaboration"></i> Tabel Siswa</h5>
  </div>
  <!-- Panel Tabel -->
  <div class="panel-body">
  <div class="well well-sm">
  <div class="box-body" style="padding-left:0px; padding-top:0px; ">
    <table id="example" class="table table-bordered" >
      <thead>
      <tr>
        <th>No.</th>
        <th>Nama</th>
        <th>Nilai UNBK</th>
        <th>Minat Siswa</th>
        <th>Saran BK</th>
        <th>Melanjutkan Ke</th>
        <th>Daerah</th>
        <th>Goal</th>
        <th>Action</th>
      </tr>
      </thead>
      <tbody>
        <?php $no=1; foreach ($getdata as $get){?>

      <tr>
        <td><?php echo $no; ?></td>
        <td><?php echo $get->nama; ?></td>
        <td><?php $string=$get->nilai_unbk; $unbk = preg_replace('/[^A-Za-z\-]/', '', $string) ;echo $unbk; ?></td>
        <td><?php $string=$get->minat_siswa; $minat = preg_replace('/[^A-Za-z\-]/', '', $string) ;echo $minat; ?></td>
        <td><?php $string=$get->saran_bk; $saran = preg_replace('/[^A-Za-z\-]/', '', $string) ;echo $saran; ?></td>
        <td><?php echo $get->melanjutkan_ke; ?></td>
        <td><?php echo $get->daerah; ?></td>
        <td><?php echo $get->goal; ?></td>
        <td style="text-align:center;">
          <a class="fa fa-edit text-blue id_datasiswa" datasiswa="<?php echo $get->id_datasiswa; ?>" data-popup="tooltip" data-original-title="Edit Data" data-placement="left"></a> &nbsp;&nbsp;
          <a class="fa fa-trash text-red" style="color:red;" onclick="return confirm('Apakah Anda Ingin Menghapus Data Ini');" href="<?php echo base_url('datasiswa/delete_datasiswa/'.$get->id_datasiswa); ?>" data-popup="tooltip" data-original-title="Hapus Data" data-placement="left"></a>
        </td>
      </tr>
      <?php $no++; }?>
    </tbody>
    </table>
  </div>
</div>
<script>
// Data table
    $('#example').dataTable({
      "paging": true,
      "searching": true,
       "scrollX": true,
         dom: 'Bfrtip',
         lengthMenu: [
           [ 10, 25, 50, -1 ],
           [ '10 rows', '25 rows', '50 rows', 'Show all' ]
       ],
       buttons: [
           'copy', 'csv', 'excel', 'pdf', 'print','pageLength'
       ]

    });
</script>

<!-- Modal update-->
<div id="modal_edit" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body">
        <div id="edit_datasiswa"></div>
      </div>
    </div>
  </div>
</div>

<!-- Modal add-->
<div id="addModals" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body">
        <div id="contAddIdent"></div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
// Modal Edit Datasiswa
$('.id_datasiswa').click(function()
{
  var datasiswa=$(this).attr('datasiswa');
  $.ajax({
    url:"<?php echo base_url('Datasiswa/modal_edit');?>",
    method:"POST",
    data:{id_datasiswa:datasiswa},
    success: function(data){
      $('#modal_edit').modal('show');
      $('#edit_datasiswa').html(data);
      $('.modal-title').html('Data Siswa');
    }
  });
});
</script>
