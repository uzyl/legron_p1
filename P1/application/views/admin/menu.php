<div class="sidebar sidebar-main" style="">
  <div class="sidebar-content">
    <div class="sidebar-user">
    </div>
    <div class="sidebar-category sidebar-category-visible">
      <div class="category-content no-padding">
        <ul class="navigation navigation-main navigation-accordion">
          <li class="<?php echo menuaktif('dashboard',$aktif); ?>"><a href="<?php echo base_url(); ?>"><i class="icon-home4"></i> <span>Dashboard</span></a></li>
          <li class="<?php echo menuaktif('datasiswa',$aktif); ?>"><a href="<?php echo base_url('datasiswa') ?>"><i class="fas fa-file-alt fa-lg"></i> <span>Data Siswa</span></a></li>
          <li class="<?php echo menuaktif('kriteria',$aktif); ?>"><a href="<?php echo site_url('kriteria'); ?>"><i class="fas fa-clone fa-lg"></i> <span>Kriteria</span></a></li>
          <li class="<?php echo menuaktif('admin',$aktif); ?>"><a href="<?php echo site_url('Admin'); ?>"><i class="icon-collaboration"></i> <span>Akses Admin</span></a></li>
          <li class="<?php echo menuaktif('hasil',$aktif); ?>"><a href="<?php echo site_url('hasil'); ?>"><i class="fas fa-clipboard fa-lg"></i> <span>Hasil</span></a></li>
          <li class="<?php echo menuaktif('peserta',$aktif); ?>"><a href="<?php echo site_url('Peserta'); ?>"><i class="fas fa-user fa-lg"></i> <span>Data User</span></a></li>
          <li class="<?php echo menuaktif('tentang',$aktif); ?>"><a href="<?php echo site_url('tentang'); ?>"><i class="fas fa-address-card fa-lg"></i> <span>Tetang Kami</span></a></li>
        </ul>
      </div>
    </div>
  </div>
</div>
