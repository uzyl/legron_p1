<!-- Main content -->
<section class="content">
  <!-- Small boxes (Stat box) -->
  <div class="row">
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-aqua">
        <div class="inner">
          <h3>30<sup style="font-size: 20px">%</sup></h3>

          <p>New Orders</p>
        </div>
        <div class="icon">
          <i class="ion ion-bag"></i>
        </div>
        <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-green">
        <div class="inner">
          <h3>50<sup style="font-size: 20px">%</sup></h3>

          <p>Bounce Rate</p>
        </div>
        <div class="icon">
          <i class="ion ion-stats-bars"></i>
        </div>
        <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-yellow">
        <div class="inner">
          <h3>20<sup style="font-size: 20px">%</sup></h3>

          <p>User Registrations</p>
        </div>
        <div class="icon">
          <i class="ion ion-person-add"></i>
        </div>
        <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-red">
        <div class="inner">
          <h3>70<sup style="font-size: 20px">%</sup></h3>

          <p>Unique Visitors</p>
        </div>
        <div class="icon">
          <i class="ion ion-pie-graph"></i>
        </div>
        <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div>
    <!-- ./col -->
  </div>

  <!-- CONTENT -->
</section>

<div class="box">
  <div><?php echo $this->session->flashdata('message'); ?></div>
  <!-- /.box-header -->
  <div class="panel panel-primary">
    <div class="panel-heading" style="    color: #fff;
      background-color: #2ECCFA;
      border-color: #2ECCFA;">
      <h5 class="panel-title"><i class="icon-collaboration"></i> Kriteria Data Siswa</h5>
  </div>
  <!-- Panel Tabel -->
  <div class="panel-body">
  <div class="well well-sm">
  <div class="box-body" style="padding-left:0px; padding-top:0px; ">
    <table id="example" class="dataTables table table-striped table-bordered" >
      <thead>
      <tr>
        <th>No.</th>
        <th>Nama</th>
        <th>Nilai UNBK</th>
        <th>Minat Siswa</th>
        <th>Saran BK</th>
        <th>Melanjutkan Ke</th>
        <th>Daerah</th>
        <!-- <th>Goal</th>
        <th>Action</th> -->
      </tr>
      </thead>

        <?php $no=1; foreach ($getdata as $get){?>

      <tr>
        <td><?php echo $no; ?></td>
        <td><?php echo $get->nama; ?></td>
        <td><?php $string=$get->nilai_unbk; $unbk = preg_replace('/[^A-Za-z\-]/', '', $string) ;echo $unbk; ?></td>
        <td><?php $string=$get->minat_siswa; $minat = preg_replace('/[^A-Za-z\-]/', '', $string) ;echo $minat; ?></td>
        <td><?php $string=$get->saran_bk; $saran = preg_replace('/[^A-Za-z\-]/', '', $string) ;echo $saran; ?></td>
        <td><?php echo $get->melanjutkan_ke; ?></td>
        <td><?php echo $get->daerah; ?></td>
        <!-- <td><?php echo $get->goal; ?></td> -->
        <!-- <td style="text-align:center;">
          <a class="fa fa-edit text-blue id_datasiswa" datasiswa="<?php echo $get->id_datasiswa; ?>" data-popup="tooltip" data-original-title="Edit Data" data-placement="left"></a> &nbsp;&nbsp;
          <a class="fa fa-trash text-red" style="color:red;" onclick="return confirm('Apakah Anda Ingin Menghapus Data Ini');" href="<?php echo base_url('datasiswa/delete_datasiswa/'.$get->id_datasiswa); ?>" data-popup="tooltip" data-original-title="Hapus Data" data-placement="left"></a>
        </td> -->
      </tr>
      <?php $no++; }?>
    </table>
  </div>
</div>

<script type="text/javascript">
// Data table
    $('#example').dataTable({
       "paging": true,
       "searching": true,
        "scrollX": true,
          dom: 'Bfrtip',
          lengthMenu: [
            [ 10, 25, 50, -1 ],
            [ '10 rows', '25 rows', '50 rows', 'Show all' ]
        ],
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print','pageLength'
        ]
    });
</script>
