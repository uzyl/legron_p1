<div class="navbar navbar-default navbar-fixed-top header-highlight">
  <div class="navbar-header">
    <a class="navbar-brand" href="<?php echo base_url(); ?>"><b style="color:white;font-size:17px;"><i>Home</i></b></a>
    <ul class="nav navbar-nav visible-xs-block">
      <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
      <li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
    </ul>
  </div>

  <div class="navbar-collapse collapse" id="navbar-mobile">
    <ul class="nav navbar-nav">
      <li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>
    </ul>
    <ul class="nav navbar-nav navbar-right">
      <li class="dropdown dropdown-user">
        <a class="dropdown-toggle" data-toggle="dropdown">
          <img src="" alt="">
            <!--<?php if($this->session->userdata('level')==1){echo "Superadmin";}else{echo "Admin";} ?>-->
            <?php echo $this->session->userdata('user') ?>
          <i class="caret"></i>
        </a>

        <ul class="dropdown-menu dropdown-menu-right bg-primary">
           <?php
              $user=$this->session->userdata('level');
              $username = $this->session->userdata('user');
              $id_peserta = $this->session->userdata('id_peserta');
              if ($user == 3){
                ?>
                  <li><a href="<?php echo site_url('aksespeserta/Account/getUser/'.$id_peserta.'/'.$username); ?>"><i class="fa fa-user"></i> Ubah Account</a></li>
                  <li><a href="<?php echo site_url('Dash/logout'); ?>" onclick="return confirm('Apakah Anda Yakin Ingin Keluar');"><i class="icon-switch2"></i> Logout</a></li>
                <?php
              }else{?>
                <li><a href="" data-toggle="modal" data-target="#ganti" hidden="true"><i class="icon-cog5"></i>Ubah Username Dan Password</a></li>
                <li><a href="<?php echo site_url('Dash/logout'); ?>" onclick="return confirm('Apakah Anda Yakin Ingin Keluar');"><i class="icon-switch2"></i> Logout</a></li>
                <?php
              }
          ?>

        </ul>
      </li>
    </ul>
  </div>
</div>
