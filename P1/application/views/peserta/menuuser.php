<div class="sidebar sidebar-main">
  <div class="sidebar-content">
    <div class="sidebar-user">
    </div>
    <div class="sidebar-category sidebar-category-visible">
      <div class="category-content no-padding">
        <ul class="navigation navigation-main navigation-accordion">
          <li class="<?php echo menuaktif('home',$aktif); ?>"><a href="<?php echo base_url('aksespeserta/Home'); ?>"><i class="icon-home4"></i> <span>Dashboard</span></a></li>
          <!-- <li class="<?php echo menuaktif('soal',$aktif); ?>"><a href="<?php echo site_url('aksespeserta/SPeserta'); ?>"  onclick="return confirm('Apakah Anda Yakin Ingin Melakukan Test Dan Test Hanya Dapat Dilakukan Satu Kali');"><i class="fa fa-exclamation"></i> <span>Soal</span></a></li> -->
          <li class="<?php echo menuaktif('proses',$aktif); ?>"><a href="<?php echo site_url('aksespeserta/proses'); ?>"><i class="fa fa-star"></i> <span>Mulai</span></a></li>
        </ul>
      </div>
    </div>
  </div>
</div>
