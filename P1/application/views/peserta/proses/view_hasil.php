<div class="panel panel-primary">
  <div class="panel-heading" style="color:#fff; background-color:#2ECCFA; border-color: #2ECCFA;">
    <h5 class="panel-title"><i class="icon-collaboration"></i> Hasil</h5>
  </div>
<div class="row">
  <div class="col-md-6">
    <div class="panel-body">
      <div class="panel-heading" style="color: #fff;
        background-color: #2ECCFA;
        border-color: #2ECCFA;">
        <h5 class="panel-title"><i class="icon-collaboration"></i> Algoritma SVM</h5>
    </div>
      <div class="well well-sm">
        <!-- Konten -->
        <form action="<?php echo site_url('aksespeserta/proses/insert_proses'); ?>" method="post">
          <input type="hidden" name="id_proses" id="id_proses" class="form-control" required>
        <div class="row">
          <div class="col-md-12">
            <center>
              <div class="form-group">
                <input type="text" name="nama" id="nama" autocomplete="off" placeholder="Nama" class="form-control" value="<?php echo $getdata[0]['nama_p'] ?>" required>
              </div>
            </center>
          </div>
          <div class="col-md-12">
            <center>
              <div class="form-group">
               <select name="nilai_unbk" id="nilai_unbk" class="select-clear" required>
                  <!-- <optgroup label="Nilai UNBK"> -->
                    <option value="">-- <?php $string=$getdata[0]['nilai_unbk_p']; $unbk = preg_replace('/[^A-Za-z\-]/', '', $string) ;echo $unbk;?> --</option>
                    <option value="1_Tinggi">Tinggi</option>
                    <option value="0.5_Sedang">Sedang</option>
                    <option value="0_Rendah">Rendah</option>
                  <!-- </optgroup> -->
              </select>
            </div>
            </center>
          </div>
          <div class="col-md-12">
            <center>
              <div class="form-group">
               <select name="minat_siswa" id="minat_siswa" class="select-clear" required>
                 <!-- <optgroup label="Minat Siswa"> -->
                   <option value="">-- <?php $string=$getdata[0]['minat_siswa_p']; $minat = preg_replace('/[^A-Za-z\-]/', '', $string) ;echo $minat;?> --</option>
                   <option value="1_SMK">SMK</option>
                   <option value="0.5_SMA">SMA</option>
                   <option value="0_MA">MA</option>
                 <!-- </optgroup> -->
              </select>
            </div>
            </center>
          </div>
          <div class="col-md-12">
            <center>
              <div class="form-group">
               <select name="saran_bk" id="saran_bk" class="select-clear" required>
                 <!-- <optgroup label="Saran BK"> -->
                   <option value="">-- <?php $string=$getdata[0]['saran_bk_p']; $saran = preg_replace('/[^A-Za-z\-]/', '', $string) ;echo $saran;?> --</option>
                   <option value="1_SMK">SMK</option>
                   <option value="0.5_SMA">SMA</option>
                   <option value="0_MA">MA</option>
                 <!-- </optgroup> -->
              </select>
            </div>
            </center>
          </div>
          <div class="col-md-12">
            <center>
              <div class="form-group">
                <input type="text" name="rekomen" id="rekomen" autocomplete="off" placeholder="Rekomendasi Sekolah Lanjutan " class="form-control" required>
              </div>
            </center>
          </div>
        </div>
        <br><br>
        <div class="row">
          <center>
            <label for="waktu">Waktu: </label>
          </center>
        </div>
      </form>
    </div>
    </div>
  </div>
  <div class="col-md-6">
    <div class="panel-body">
      <div class="panel-heading" style="color: #fff;
        background-color: #2ECCFA;
        border-color: #2ECCFA;">
        <h5 class="panel-title"><i class="icon-collaboration"></i> Algoritma KNN</h5>
    </div>
      <div class="well well-sm">
        <!-- Konten -->
        <form action="<?php echo site_url('aksespeserta/proses/insert_proses'); ?>" method="post">
          <input type="hidden" name="id_proses" id="id_proses" class="form-control" required>
        <div class="row">
          <div class="col-md-12">
            <center>
              <div class="form-group">
                <input type="text" name="nama" id="nama" autocomplete="off" placeholder="Nama" class="form-control" value="<?php echo $getdata[0]['nama_p'] ?>" required>
              </div>
            </center>
          </div>
          <div class="col-md-12">
            <center>
              <div class="form-group">
               <select name="nilai_unbk" id="nilai_unbk" class="select-clear" required>
                  <!-- <optgroup label="Nilai UNBK"> -->
                    <option value="">-- <?php $string=$getdata[0]['nilai_unbk_p']; $unbk = preg_replace('/[^A-Za-z\-]/', '', $string) ;echo $unbk;?> --</option>
                    <option value="1_Tinggi">Tinggi</option>
                    <option value="0.5_Sedang">Sedang</option>
                    <option value="0_Rendah">Rendah</option>
                  <!-- </optgroup> -->
              </select>
            </div>
            </center>
          </div>
          <div class="col-md-12">
            <center>
              <div class="form-group">
               <select name="minat_siswa" id="minat_siswa" class="select-clear" required>
                 <!-- <optgroup label="Minat Siswa"> -->
                   <option value="">-- <?php $string=$getdata[0]['minat_siswa_p']; $minat = preg_replace('/[^A-Za-z\-]/', '', $string) ;echo $minat;?> --</option>
                   <option value="1_SMK">SMK</option>
                   <option value="0.5_SMA">SMA</option>
                   <option value="0_MA">MA</option>
                 <!-- </optgroup> -->
              </select>
            </div>
            </center>
          </div>
          <div class="col-md-12">
            <center>
              <div class="form-group">
               <select name="saran_bk" id="saran_bk" class="select-clear" required>
                 <!-- <optgroup label="Saran BK"> -->
                   <option value="">-- <?php $string=$getdata[0]['saran_bk_p']; $saran = preg_replace('/[^A-Za-z\-]/', '', $string) ;echo $saran;?> --</option>
                   <option value="1_SMK">SMK</option>
                   <option value="0.5_SMA">SMA</option>
                   <option value="0_MA">MA</option>
                 <!-- </optgroup> -->
              </select>
            </div>
            </center>
          </div>
          <div class="col-md-12">
            <center>
              <div class="form-group">
                <input type="text" name="rekomen" id="rekomen" autocomplete="off" placeholder="Rekomendasi Sekolah Lanjutan " class="form-control" required>
              </div>
            </center>
          </div>
        </div>
        <br><br>
        <div class="row">
          <center>
            <label for="waktu">Waktu: </label>
          </center>
        </div>
      </form>
    </div>
    </div>
  </div>
</div>

<div class="box">


<div class="row" style="margin-bottom:20px;">
    <center>
      <!-- <input id="insert_datasiswa" value="Tambah" type="submit" class="btn btn-warning btn-sm"><i class="icon-file-plus"></i>> -->
      <button type="button" class="btn btn-danger btn-sm" onclick="location.href='<?php echo base_url().'aksespeserta/export/Export_pdf/'.$getdata[0]['id_proses']; ?> '"><i class="fas fa-print lg" ></i> Export PDF </button>
    </center>
</div>
</div>
