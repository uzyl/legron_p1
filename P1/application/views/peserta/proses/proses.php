<div class="panel panel-primary">
  <div class="panel-heading" style="color: #fff;
    background-color: #2ECCFA;
    border-color: #2ECCFA;">
    <h5 class="panel-title"><i class="icon-collaboration"></i> Form Proses</h5>
</div>

<div class="row">
  <div class="col-md-4">
    <div class="panel-body">
      <div class="panel-heading" style="color: #fff;
        background-color: #2ECCFA;
        border-color: #2ECCFA;">
        <h5 class="panel-title"><i class="icon-collaboration"></i> Keterangan</h5>
    </div>
      <div class="well well-sm">

      <h5>Harap isi dengan data yang benar!</h5>
      <ul>
        <li> <p>Nama sesuai ijazah</p> </li>
        <li> <p>Nilai merupakan nilai rata-rata dari keseluruhan nilai</p> </li>
        <li> <p>Minat Siswa: SMK/SMA/MA</p> </li>
        <li> <p>Saran BK: SMK/SMA/MA</p> </li>
      </ul>
    </div>
    </div>
  </div>
  <div class="col-md-8">
    <div class="panel-body">
      <div class="well well-sm">
      <!-- Konten -->
      <form action="<?php echo site_url('aksespeserta/proses/insert_proses/'); ?>" method="post">
        <input type="hidden" name="id_proses" id="id_proses" class="form-control" required>
      <div class="row">
        <div class="col-md-6">
          <!-- <input type="hidden" name="id_datasiswa" id="id_datasiswa" required> -->
          <center>
            <div class="form-group">
              <input type="text" name="nama" id="nama" autocomplete="off" placeholder="Nama" class="form-control" required>
            </div>
          </center>
        </div>
        <div class="col-md-6">
          <center>
            <div class="form-group">
             <select name="nilai_unbk" id="nilai_unbk" class="select-clear" required>
                <!-- <optgroup label="Nilai UNBK"> -->
                  <option value="">-- Nilai UNBK --</option>
                  <option value="1_Tinggi">Tinggi</option>
                  <option value="0.5_Sedang">Sedang</option>
                  <option value="0_Rendah">Rendah</option>
                <!-- </optgroup> -->
            </select>
          </div>
          </center>
        </div>
        <div class="col-md-6">
          <center>
            <div class="form-group">
             <select name="minat_siswa" id="minat_siswa" class="select-clear" required>
               <!-- <optgroup label="Minat Siswa"> -->
                 <option value="">-- Minat Siswa --</option>
                 <option value="1_SMK">SMK</option>
                 <option value="0.5_SMA">SMA</option>
                 <option value="0_MA">MA</option>
               <!-- </optgroup> -->
            </select>
          </div>
          </center>
        </div>
        <div class="col-md-6">
          <center>
            <div class="form-group">
             <select name="saran_bk" id="saran_bk" class="select-clear" required>
               <!-- <optgroup label="Saran BK"> -->
                 <option value="">-- Saran BK --</option>
                 <option value="1_SMK">SMK</option>
                 <option value="0.5_SMA">SMA</option>
                 <option value="0_MA">MA</option>
               <!-- </optgroup> -->
            </select>
          </div>
          </center>
        </div>
      </div>
      <br><br>
      <div class="row">
          <center>
            <!-- <input id="insert_datasiswa" value="Tambah" type="submit" class="btn btn-warning btn-sm"><i class="icon-file-plus"></i>> -->
            <button type="submit" id="insert_datasiswa" class="btn btn-info btn-sm"><i class="icon-file-plus"></i> Proses </button>
          </center>
      </div>
    </form>
    </div>
  </div>
</div>
