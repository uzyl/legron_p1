<!DOCTYPE html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Laporan</title>
  <link href="<?php echo base_url('assets/css/minified/bootstrap.min.css'); ?>" rel="stylesheet" type="text/css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/fontawesome/font-awsome.min.css'); ?>">
</head>
<style type="text/css">
    th{
      font-family: Times;
      font-size: 12px;
      font-weight: bold;
    }
    td{
      font-family: Times;
      font-size: 11px;
    }
  </style>

<!-- P1 -->
<div class="panel panel-primary">
  <div class="panel-heading" style="color:#fff; background-color:#2ECCFA; border-color: #2ECCFA;">
    <center>
    <h5 class="panel-title"><i class="icon-collaboration"></i> Laporan Hasil</h5>
  </center>
  </div>
<div class="row">
  <div class="col-md-6">
    <div class="panel-body">
      <p>Hasil perbandingan antara algoritma SVM dan algoritma KNN untuk merekomendasikan jenjang pendidikan selanjutnya dapat dilihat pada dua tabel dibawah:</p>
      <div class="panel-heading" style="color:#fff; background-color:#2ECCFA; border-color: #2ECCFA;">
        <h5 class="panel-title"><i class="icon-collaboration"></i> Algoritma SVM</h5>
    </div>
    <div class="well well-sm">
        <!-- Konten -->
        <div role="tabpanel" class="tab-pane fade active in" id="tab_content3" aria-labelledby="home-tab">
          <table class="table table-striped">
              <tr>
                <td>Nama: </td>
                <td><?php echo $getdata[0]['nama_p'] ?></td>
              </tr>
              <tr>
                <td>Nilai UNBK: </td>
                <td><?php $string=$getdata[0]['nilai_unbk_p']; $unbk = preg_replace('/[^A-Za-z\-]/', '', $string) ;echo $unbk;?></td>
              </tr>
              <tr>
                <td>Minat Siswa: </td>
                <td><?php $string=$getdata[0]['minat_siswa_p']; $minat = preg_replace('/[^A-Za-z\-]/', '', $string) ;echo $minat;?></td>
              </tr>
              <tr>
                <td>Saran BK: </td>
                <td><?php $string=$getdata[0]['saran_bk_p']; $saran = preg_replace('/[^A-Za-z\-]/', '', $string) ;echo $saran;?></td>
              </tr>
              <tr>
                <td>Rekomendasi: </td>
                <td><?php echo $getdata[0]['rekomendasi'] ?></td>
              </tr>
          </table>
        </div>
    </div>
    </div>
  </div>
  <div class="col-md-6">
    <div class="panel-body">
      <div class="panel-heading" style="color:#fff; background-color:#2ECCFA; border-color: #2ECCFA;">
        <h5 class="panel-title"><i class="icon-collaboration"></i> Algoritma KNN</h5>
    </div>
    <div class="well well-sm">
        <!-- Konten -->
        <div role="tabpanel" class="tab-pane fade active in" id="tab_content3" aria-labelledby="home-tab">
          <table class="table table-striped">
              <tr>
                <td>Nama: </td>
                <td><?php echo $getdata[0]['nama_p'] ?></td>
              </tr>
              <tr>
                <td>Nilai UNBK: </td>
                <td><?php $string=$getdata[0]['nilai_unbk_p']; $unbk = preg_replace('/[^A-Za-z\-]/', '', $string) ;echo $unbk;?></td>
              </tr>
              <tr>
                <td>Minat Siswa: </td>
                <td><?php $string=$getdata[0]['minat_siswa_p']; $minat = preg_replace('/[^A-Za-z\-]/', '', $string) ;echo $minat;?></td>
              </tr>
              <tr>
                <td>Saran BK: </td>
                <td><?php $string=$getdata[0]['saran_bk_p']; $saran = preg_replace('/[^A-Za-z\-]/', '', $string) ;echo $saran;?></td>
              </tr>
              <tr>
                <td>Rekomendasi: </td>
                <td><?php echo $getdata[0]['rekomendasi'] ?></td>
              </tr>
          </table>
        </div>
    </div>
    </div>
  </div>
</div>

<script type="text/javascript" src="<?php echo base_url('assets/js/core/libraries/jquery.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/core/libraries/bootstrap.min.js'); ?>"></script>

</html>
