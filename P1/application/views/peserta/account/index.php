<?php 
$data=$this->session->flashdata('sukses');
if($data!=""){ ?>
<div class="alert alert-success"><strong>Sukses! </strong> <?=$data;?></div>
<?php } ?>
<?php 
$data2=$this->session->flashdata('error');
if($data2!=""){ ?>
<div class="alert alert-danger"><strong> Error! </strong> <?=$data2;?></div>
<?php } ?>


<div class="panel panel-primary">
  <div class="panel-heading">
    <h5 class="panel-title"><i class="icon-people"></i> Data Peserta</h5>
  </div>
  <div class="panel-body">
  <div class="well well-sm">
  	<?php $no=0; foreach($all as $row): $no++; ?>
       <form action="<?php echo site_url('aksespeserta/Account/edit/'.$row->id_peserta); ?>" method="post">
        <div class="row">
          <div class="col-xs-2 col-sm-6">
            <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-user"> Nama Lengkap</i></span>
              <input type="text" name="nama" id="nama" class="form-control" value="<?php echo $row->nama; ?>" placeholder="nama" required>
            </div>
          </div>
          <div class="col-xs-2 col-sm-6">
            <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-calendar"> Tanggal Lahir</i></span>
              <input type="date" value="<?php echo $row->tgl_lahir; ?>" class="form-control" style="color:black; height:35px;" id="tgl_lahir" name="tgl_lahir" placeholder="Username" required>
            </div>
          </div>
        </div></br>
        <div class="row">
          <div class="col-xs-2 col-sm-6">
            <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-phone"> Hp</i> </span>
              <input type="text" value="<?php echo $row->hp; ?>"  pattern="[0-9]*" minlength="11" maxlength="12" name="hp" id="hp" placeholder="No Hp" class="form-control" required>
          </div>
          </div>
          <div class="col-xs-2 col-sm-6">
            <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-envelope"> Email</i></span>
              <input type="email" value="<?php echo $row->email; ?>" name="email" id="email" placeholder="Email" class="form-control" required>
          </div>
          </div>
        </div><br>
        <div class="row">
          <div class="col-xs-2 col-sm-6">
            <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-road"> Alamat</i></span>
               <textarea name="alamat" id="alamat" placeholder="Alamat" class="form-control" required><?php echo $row->alamat; ?></textarea>
          </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <span class="input-group-addon"><i class="fa fa-users"> Jenis Kelamin</i></span>
                <select name="jenis_kelamin" class="select-clear" required>
                  <option value="">-- Pilih Jenis Kelamin --</option>
                  <option <?php if ($row->jenis_kelamin == 'Laki-laki'){echo "selected";} ?> value="Laki-laki">Laki-laki</option>
                  <option <?php if ($row->jenis_kelamin == 'Perempuan'){echo "selected";} ?> value="Perempuan">Perempuan</option>
                  </select>
              </div>
            </div>
          </div>
        </div><br>
          <!--  -->
          <div class="row">
          	<div class="pull-right">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-warning"><i class="icon-pencil5"></i> Edit</button>
            </div>
           </div>  
        </form>
        <?php endforeach; ?>
        <br>
        <?php $no=0; foreach($all as $row): $no++; ?>
        <form action="<?php echo site_url('aksespeserta/Account/editAcc'); ?>" method="post">
          <div class="modal-content">
            <div class="modal-header bg-primary">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h6 class="modal-title"><strong>Edit Account</strong></h6>
            </div>
            <div class="modal-body">
            
        <div class="row">
        	<?php 
                    $user=$this->session->userdata('user');
                    $qu=$this->db->query("select * from peserta where username='$user'")->row_array(); ?>
                    <div class="form-group">
                      <label class='col-md-3'>Username</label>
                      <div class='col-md-9'><input value="<?php echo $qu['username']; ?>" type="text" name="username" placeholder="Masukkan Username" class="form-control" ></div>
                    </div>
                    <br>
                    
                    <input type="hidden" name="passlama" value="<?php echo $qu['password']; ?>">
                    <input type="hidden" name="userlama" value="<?php echo $qu['username']; ?>">
                    <div class="form-group">
                      <label class='col-md-3'>Password Lama</label>
                      <div class='col-md-9'><input type="password" name="password" autocomplete="off" required placeholder="Masukkan Password" class="form-control" ></div>
                    </div>
                    <br>
                    <div class="form-group">
                      <label class='col-md-3'>Password Baru</label>
                      <div class='col-md-9'><input type="password" name="password_baru" autocomplete="off" required placeholder="Masukkan Password Baru" class="form-control" ></div>
                    </div>
                    <br>
                    <div class="form-group">
                      <label class='col-md-3'>Konfirmasi Password Baru</label>
                      <div class='col-md-9'><input type="password" name="konf_baru" autocomplete="off" required placeholder="Masukkan Konfirmasi Password Baru" class="form-control" ></div>
                    </div>
                    <br>
                     <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-warning"><i class="icon-pencil5"></i> Edit</button>
            </div>  
        </div>
          </form>
  <?php endforeach; ?>
</div>
  <br>
  </div>
</div>



