<?php

class Kriteria extends CI_Controller {
	function __construct(){
		parent::__construct();
    $this->load->helper('url');
		$this->load->model('M_datasiswa');
		$config['tag_open'] = '<ul class="breadcrumb">';
		$config['tag_close'] = '</ul>';
		$config['li_open'] = '<li>';
		$config['li_close'] = '</li>';
		$config['divider'] = '<span class="divider"> » </span>';
		$this->breadcrumb->initialize($config);
		no_access();
		levelsuper();
	}

	public function index()
	{
    $data=array(
			"title"=>'Kriteria',
			"menu"=>getmenu(),
			"aktif"=>"kriteria",
			"content"=>"kriteria/kriteria.php",
		);
		$data['getdata'] = $this->M_datasiswa->get_datasiswa('datasiswa');
		$this->breadcrumb->append_crumb('Kriteria', site_url('kriteria'));
		$this->load->view('admin/template',$data);
	}

}
