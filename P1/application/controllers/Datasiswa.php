<?php

class Datasiswa extends CI_Controller {
	function __construct(){
		parent::__construct();
    $this->load->helper('url');
		$this->load->model('M_datasiswa');
		$config['tag_open'] = '<ul class="breadcrumb">';
		$config['tag_close'] = '</ul>';
		$config['li_open'] = '<li>';
		$config['li_close'] = '</li>';
		$config['divider'] = '<span class="divider"> » </span>';
		$this->breadcrumb->initialize($config);
		no_access();
		levelsuper();
	}

	public function index()
	{
    $data=array(
			"title"=>'Data Siswa',
			"menu"=>getmenu(),
			"aktif"=>"datasiswa",
			"content"=>"datasiswa/datasiswa.php",
		);
		$data['getdata'] = $this->M_datasiswa->get_datasiswa('datasiswa');
		$this->breadcrumb->append_crumb('Data Siswa', site_url('datasiswa'));
		$this->load->view('admin/template',$data);
	}

	public function insert_datasiswa()
	{
		$data['nama']=$this->input->post('nama');
		$data['nilai_unbk']=$this->input->post('nilai_unbk');
		$data['minat_siswa']=$this->input->post('minat_siswa');
		$data['saran_bk']=$this->input->post('saran_bk');
		$data['melanjutkan_ke']=$this->input->post('melanjutkan_ke');
		$data['daerah']=$this->input->post('daerah');
		// $data=array(
		// 	'status'=>1,
		// );
		$this->M_datasiswa->insert_datasiswa('datasiswa',$data);
		$this->session->set_flashdata('sukses', 'Data Berhasil Di Tambahkan');
		redirect('datasiswa');
	}

	public function modal_edit()
	{
		$id=$this->input->post('id_datasiswa');
		$where['id_datasiswa']=$id;
		$datasiswa=$this->M_datasiswa->getwhere_datasiswa('datasiswa',$where);
		$data['datasiswa']=$datasiswa;
		$this->load->view('admin/datasiswa/edit',$data);
	}

	public function update_datasiswa()
	{
		$data['nama']=$this->input->post('nama');
		$data['nilai_unbk']=$this->input->post('nilai_unbk');
		$data['minat_siswa']=$this->input->post('minat_siswa');
		$data['saran_bk']=$this->input->post('saran_bk');
		$data['melanjutkan_ke']=$this->input->post('melanjutkan_ke');
		$data['daerah']=$this->input->post('daerah');

		$where['id_datasiswa']=$this->input->post('id_datasiswa');
		$result=$this->M_datasiswa->update_datasiswa('datasiswa',$data,$where);

		if (!empty($result)) {
			$this->session->set_flashdata('message','
			<div class="alert alert-success">
  		<strong>Success!</strong> Update data berhasil.
			</div>');
			redirect('datasiswa');
		}
		else {
			$this->session->set_flashdata('message','
			<div class="alert alert-danger">
  		<strong>Maaf</strong> Update data gagal.
			</div>');
			redirect('datasiswa/modal_edit');
		}
	}

	public function delete_datasiswa($id)
	{
		$where['id_datasiswa']=$id;
		$result=$this->M_datasiswa->delete_datasiswa('datasiswa',$where);
		if ($result) {
			$this->session->set_flashdata('message','<div class="alert alert-success" role="alert">Data Berhasil Dihapus</div>');
			redirect('datasiswa');
		}
		else {
			$this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
		  <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
		  <span class="sr-only">Error:</span>
		  Data Gagal Dihapus!
			</div>');
			redirect('admin/identitas');
		}
	}
}
