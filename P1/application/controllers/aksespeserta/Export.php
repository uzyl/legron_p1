<?php

class Export extends CI_Controller {
	function __construct(){

		parent::__construct();
		$this->load->model('M_proses');
		$this->load->helper('url');
		$this->load->helper('html');
		$config['tag_open'] = '<ul class="breadcrumb">';
		$config['tag_close'] = '</ul>';
		$config['li_open'] = '<li>';
		$config['li_close'] = '</li>';
		$config['divider'] = '<span class="divider"> » </span>';
		$this->breadcrumb->initialize($config);
		no_access();
		levelpesert();
	}
	public function index()
	{
		// $data=array(
		// 	// "title"=>'Proses Data',
		// 	// "menu"=>getmenu(),
		// 	// "aktif"=>"proses",
		// 	"content"=>"proses/export.php",
		// );
    // $where = $id;
		// $data['getdata'] = $this->M_proses->get_proses('proses',$where);
		// // $this->breadcrumb->append_crumb('Proses Data', site_url('aksespeserta/proses'));
		// $this->load->view('peserta/template',$data);
	}
  public function laporan_pdf(){

    $data = array(
        "dataku" => array(
            "nama" => "Petani Kode",
            "url" => "http://petanikode.com"
        )
    );

    $this->load->library('pdf');

    $this->pdf->setPaper('A4', 'potrait');
    $this->pdf->filename = "laporan-petanikode.pdf";
    $this->pdf->load_view('laporan_pdf', $data);
  }

  public function Export_pdf($id)
  {
    // $data=array(
		// 	// "title"=>'Proses Data',
		// 	// "menu"=>getmenu(),
		// 	// "aktif"=>"proses",
		// 	"content"=>"proses/export.php",
		// );
    $this->load->library('pdf');
    $this->pdf->setPaper('A4', 'potrait');
    $this->pdf->filename = "laporan_hasil.pdf";
    $where['id_proses'] = $id;
		$data['getdata'] = $this->M_proses->export('proses',$where);
    $this->pdf->load_view("peserta/proses/export.php", $data);
  }

}
