<?php

class Proses extends CI_Controller {
	function __construct(){

		parent::__construct();
		$this->load->model('M_proses');
		$this->load->helper('url');
		$this->load->helper('html');
		$config['tag_open'] = '<ul class="breadcrumb">';
		$config['tag_close'] = '</ul>';
		$config['li_open'] = '<li>';
		$config['li_close'] = '</li>';
		$config['divider'] = '<span class="divider"> » </span>';
		$this->breadcrumb->initialize($config);
		no_access();
		levelpesert();
	}
	public function index()
	{
		$data=array(
			"title"=>'Proses Data',
			"menu"=>getmenu(),
			"aktif"=>"proses",
			"content"=>"proses/proses.php",
		);
		// $data['getdata'] = $this->M_datasiswa->get_datasiswa('datasiswa');
		$this->breadcrumb->append_crumb('Proses Data', site_url('aksespeserta/proses'));
		$this->load->view('peserta/template',$data);
	}

	public function view_hasil()
	{
		$data=array(
			"title"=>'Proses Data',
			"menu"=>getmenu(),
			"aktif"=>"hasil",
			"content"=>"proses/view_hasil.php",
		);
		$id=$this->M_proses->get_id()->result_array();
		$id_proses=$id[0]['id_proses'];
		$where['id_proses']=$id_proses;
		$data['getdata'] = $this->M_proses->getwhere_proses('proses',$where);
		$this->breadcrumb->append_crumb('Hasil', site_url('aksespeserta/proses'));
		$this->load->view('peserta/template',$data);
		// if ($id > 0) {
		// 	echo "ada:". $id[0]['id_proses'];
		// }
		// else {
		// 	echo "kosong";
		// }

	}

	public function insert_proses()
	{
		$data['nama_p']=$this->input->post('nama');
		$data['nilai_unbk_p']=$this->input->post('nilai_unbk');
		$data['minat_siswa_p']=$this->input->post('minat_siswa');
		$data['saran_bk_p']=$this->input->post('saran_bk');

		$result=$this->M_proses->insert_proses('proses',$data);
		$this->session->set_flashdata('sukses', 'Data Berhasil Di Tambahkan');
		redirect('aksespeserta/proses/view_hasil');
		// print $nama_p[0];
	}

}
