<?php

class Account extends CI_Controller {
	function __construct(){

		parent::__construct();
		$this->load->model('M_peserta');
		$this->load->helper('url');
		$config['tag_open'] = '<ul class="breadcrumb">';
		$config['tag_close'] = '</ul>';
		$config['li_open'] = '<li>';
		$config['li_close'] = '</li>';
		$config['divider'] = '<span class="divider"> » </span>';
		$this->breadcrumb->initialize($config);
		no_access();
		levelpesert();
	}
	public function index()
	{
		$data=array(
			"title"=>'Manajemen Akses',
			"menu"=>getmenu(),
			// "all"=>$this->M_peserta->getPeserta(),
			"aktif"=>"",
			"content"=>"account/index.php",
		);
		$this->breadcrumb->append_crumb('Ubah Account', site_url('aksespeserta/Account'));
		$this->load->view('peserta/template',$data);
	}

	public function getUser($id_peserta,$username){
		$data=array(
			"title"=>'Manajemen Akses',
			"menu"=>getmenu(),
			"all"=>$this->M_peserta->getOneId($id_peserta),
			"aktif"=>"",
			"content"=>"account/index.php",
		);
		$this->breadcrumb->append_crumb('Ubah Account', site_url('aksespeserta/Account'));
		$this->load->view('peserta/template',$data);
	}

	public function edit($id)
	{
			$data=array(
				"nama"=>$_POST['nama'],
				"tgl_lahir"=>$_POST['tgl_lahir'],
				"hp"=>$_POST['hp'],
				"email"=>$_POST['email'],
				"alamat"=>$_POST['alamat'],	
				"jenis_kelamin"=>$_POST['jenis_kelamin']
			);
			$this->M_peserta->editPeserta($id,$data);
			$this->session->set_flashdata('sukses', 'Data Peserta Berhasil Di Edit');
			redirect('aksespeserta/Home/');
	}

	public function editAcc()
	{
		$passlama=$_POST['passlama'];
		$userlama=$_POST['userlama'];
		$user=$_POST['username'];
		$password=$_POST['password'];
		$passwordbaru=$_POST['password_baru'];
		$konfbaru=$_POST['konf_baru'];
		$arr=$this->db->query("select * from peserta where username='$userlama'")->row_array();
		$cek=$this->db->query("select * from peserta where username='$user'")->num_rows();
		if($userlama!=$user){
			if($cek>0){
				$this->session->set_flashdata('error', 'Maaf Username Sudah Di Gunakan');
				redirect('aksespeserta/Home/');
			}elseif(md5($password)!=$arr['password']){
				$this->session->set_flashdata('error', 'Maaf Password Lama Anda Salah');
				redirect('aksespeserta/Home/');
			}elseif($passwordbaru!=$konfbaru){
				$this->session->set_flashdata('error', 'Maaf Konfirmasi Password Tidak Sama');
				redirect('aksespeserta/Home/');
			}elseif($passlama!=md5($passwordbaru)){
				$data=array(
					"username"=>$user,
					"password"=>md5($passwordbaru),
				);
			}else{
				$data=array(
					"username"=>$user,
					"password"=>md5($passwordbaru),
				);
			}
		}elseif(md5($password)!=$arr['password']){
			$this->session->set_flashdata('error', 'Maaf Password Lama Anda Salah');
			redirect('aksespeserta/Home/');
		}elseif($passwordbaru!=$konfbaru){
			$this->session->set_flashdata('error', 'Maaf Konfirmasi Password Tidak Sama');
			redirect('aksespeserta/Home/');
		}elseif($passlama!=md5($passwordbaru)){
			$data=array(
				"username"=>$user,
				"password"=>md5($passwordbaru),
			);
		}else{
			$data=array(
				"username"=>$user,
				"password"=>md5($passwordbaru),
			);
		}
		$this->db->where('username', $userlama);
		$this->db->update('peserta', $data);
		$this->session->set_flashdata('sukses', 'Data Berhasil Di Update');
		redirect('aksespeserta/Home/logout');
	}
}
