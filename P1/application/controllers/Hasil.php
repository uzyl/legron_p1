<?php

class Hasil extends CI_Controller {
	function __construct(){
		parent::__construct();
    $this->load->helper('url');
		$this->load->model('M_proses');
		$config['tag_open'] = '<ul class="breadcrumb">';
		$config['tag_close'] = '</ul>';
		$config['li_open'] = '<li>';
		$config['li_close'] = '</li>';
		$config['divider'] = '<span class="divider"> » </span>';
		$this->breadcrumb->initialize($config);
		no_access();
		levelsuper();
	}

	public function index()
	{
    $data=array(
			"title"=>'hasil',
			"menu"=>getmenu(),
			"aktif"=>"hasil",
			"content"=>"hasil/hasil.php",
		);
		$data['getdata'] = $this->M_proses->get_proses('proses');
		$this->breadcrumb->append_crumb('hasil', site_url('hasil'));
		$this->load->view('admin/template',$data);
	}

}
