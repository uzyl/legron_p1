<?php

class Peserta extends CI_Controller {
	function __construct(){

		parent::__construct();
		$this->load->model('M_peserta');
		$this->load->helper('url');
		$config['tag_open'] = '<ul class="breadcrumb">';
		$config['tag_close'] = '</ul>';
		$config['li_open'] = '<li>';
		$config['li_close'] = '</li>';
		$config['divider'] = '<span class="divider"> » </span>';
		$this->breadcrumb->initialize($config);
		no_access();
		levelsuper();
	}

	public function index()
	{
		$data=array(
			"title"=>'Data User',
			"menu"=>getmenu(),
			"aktif"=>"peserta",
			"all"=>$this->M_peserta->getPeserta(),
			"content"=>"peserta/index.php",
		);
		$this->breadcrumb->append_crumb('Data User', site_url('Peserta'));
		$this->load->view('admin/template',$data);
	}

	public function add()
	{
		$data=array(
				"nama"=>$_POST['nama'],
				"jenis_kelamin"=>$_POST['jk'],
				"username"=>$_POST['nama'],
				"password"=>md5($_POST['nama']),
				"tgl_lahir"=>$_POST['tgl_lahir'],
				"hp"=>$_POST['hp'],
				"email"=>$_POST['email'],
				"alamat"=>$_POST['alamat'],
				"identity_peserta"=>$_POST['identitas']
			);
			$this->M_peserta->postPeserta($data);
			$this->session->set_flashdata('sukses', 'Data Berhasil Di Tambahkan');
			redirect('Peserta');
	}

	public function EditPs($id)
	{
		$hasil = $this->M_peserta->getOneIds($id);
		$data=array(
			"title"=>'Edit Data Peserta',
			"hasil"=>$hasil,
			"menu"=>getmenu(),
			"aktif"=>"peserta",
			"all"=>$this->M_peserta->getPeserta(),
			"content"=>"peserta/indexEdit.php",
		);
		$this->breadcrumb->append_crumb('Edit Data Peserta', site_url('Peserta/edit/'.$id));
		$this->load->view('admin/template',$data);
	}

	public function edit($id)
	{
			$data=array(
				"nama"=>$_POST['nama'],
				"jenis_kelamin"=>$_POST['jns_kelamin'],
				"tgl_lahir"=>$_POST['tgl_lahir'],
				"hp"=>$_POST['hp'],
				"email"=>$_POST['email'],
				"alamat"=>$_POST['alamat']
			);
			$this->M_peserta->editPeserta($id,$data);
			$this->session->set_flashdata('sukses', 'Data Peserta Berhasil Di Edit');
			redirect('Peserta');
	}

	public function editAcc($id)
	{
			$data=array(
				"username"=>$_POST['username'],
				"password"=>md5($_POST['password'])
			);
			$this->M_peserta->editPeserta($id,$data);
			$this->session->set_flashdata('sukses', 'Data Account Berhasil Di Edit');
			redirect('Peserta');
	}

	public function hapus($id)
	{
			$this->M_peserta->hapusPeserta($id);
			$this->session->set_flashdata('sukses', 'Data Berhasil Di Hapus');
			redirect('Peserta');
	}

	public function editAccc()
	{
		$passlama=$_POST['passlama'];
		$userlama=$_POST['userlama'];
		$user=$_POST['username'];
		$password=$_POST['password'];
		$passwordbaru=$_POST['password_baru'];
		$konfbaru=$_POST['konf_baru'];
		$arr=$this->db->query("select * from peserta where username='$userlama'")->row_array();
		$cek=$this->db->query("select * from peserta where username='$user'")->num_rows();
		if($userlama!=$user){
			if($cek>0){
				$this->session->set_flashdata('error', 'Maaf Username Sudah Di Gunakan');
				redirect('Peserta');
			}elseif(md5($password)!=$arr['password']){
				$this->session->set_flashdata('error', 'Maaf Password Lama Anda Salah');
				redirect('Peserta');
			}elseif($passwordbaru!=$konfbaru){
				$this->session->set_flashdata('error', 'Maaf Konfirmasi Password Tidak Sama');
				redirect('Peserta');
			}elseif($passlama!=md5($passwordbaru)){
				$data=array(
					"username"=>$user,
					"password"=>md5($passwordbaru),
				);
			}else{
				$data=array(
					"username"=>$user,
					"password"=>md5($passwordbaru),
				);
			}
		}elseif(md5($password)!=$arr['password']){
			$this->session->set_flashdata('error', 'Maaf Password Lama Anda Salah');
			redirect('Peserta');
		}elseif($passwordbaru!=$konfbaru){
			$this->session->set_flashdata('error', 'Maaf Konfirmasi Password Tidak Sama');
			redirect('Peserta');
		}elseif($passlama!=md5($passwordbaru)){
			$data=array(
				"username"=>$user,
				"password"=>md5($passwordbaru),
			);
		}else{
			$data=array(
				"username"=>$user,
				"password"=>md5($passwordbaru),
			);
		}
		$this->db->where('username', $userlama);
		$this->db->update('peserta', $data);
		$this->session->set_flashdata('sukses', 'Data Berhasil Di Update');
		redirect('Peserta');
	}
}
