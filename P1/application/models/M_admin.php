<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_admin extends CI_Model {
	var $tabel    = 'admin';
	function __construct()
	{
		parent::__construct();
	}
	
	function getAdmin()
	{
	return	$this->db->query("select * from admin")->result();
	}
	function postAdmin($data)
	{
	return $this->db->insert($this->tabel, $data);
	}

	function editAdmin($id,$data){
		$this->db->where('id_admin', $id);
		$this->db->update('admin', $data);
	}

}

/* End of file M_entry.php */
/* Location: ./application/models/M_entry.php */