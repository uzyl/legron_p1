<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_peserta extends CI_Model {
	var $tabel    = 'peserta';
	function __construct()
	{
		parent::__construct();
	}
	
	function getPeserta()
	{
	return	$this->db->query("select * from peserta")->result();
	}

	function postPeserta($data)
	{
		return $this->db->insert($this->tabel, $data);
	}

	function editPeserta($id,$data){
		$this->db->where('id_peserta', $id);
		$this->db->update('peserta', $data);
	}

	function hapusPeserta($id){
		$this->db->where('id_peserta', $id);
			$this->db->delete('peserta');
	}

	function getUsername($username){
		return $this->db->query("select * from peserta where username='$username'")->result();
	}

	function getOneUsername($username){
		return $this->db->query("select * from peserta where username='$username'")->row_array();
	}

	function getOneIds($id){
		return $this->db->query("select * from peserta where id_peserta='$id'")->row_array();
	}

	function getOneId($id){
		return $this->db->query("select * from peserta where id_peserta='$id'")->result();
	}

}

/* End of file M_entry.php */
/* Location: ./application/models/M_entry.php */