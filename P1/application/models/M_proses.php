<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_proses extends CI_Model {

	function __construct()
	{
		parent::__construct();
	}

	function get_id()
	{
	$this->db->select('id_proses');
  $this->db->from('proses');
  $this->db->order_by("id_proses", "desc");
  return $this->db->get();
	}

	function insert_proses($tabel,$data)
	{
		return $this->db->insert($tabel,$data);
	}

	function getwhere_proses($tabel,$where)
	{
		return $this->db->get_where($tabel,$where)->result_array();
	}

	function get_proses($tabel)
	{
		return $this->db->get($tabel)->result();
	}

	public function export($tabel, $where)
	{
		return $this->db->get_where($tabel, $where)->result_array();
	}

	// function update_datasiswa($tabel,$data,$where)
	// {
	// 	return $this->db->update($tabel,$data,$where);
	// }
  //
	// function delete_datasiswa($tabel,$where)
	// {
	// 	return $this->db->delete($tabel,$where);
	// }

}

/* End of file M_entry.php */
/* Location: ./application/models/M_entry.php */
