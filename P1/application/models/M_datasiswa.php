<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_datasiswa extends CI_Model {

	function __construct()
	{
		parent::__construct();
	}

	function get_datasiswa($tabel)
	{
	return	$this->db->get($tabel)->result();
	}

	function insert_datasiswa($tabel,$data)
	{
		return $this->db->insert($tabel,$data);
	}

	function getwhere_datasiswa($tabel,$where)
	{
		return $this->db->get_where($tabel,$where)->result_array();
	}

	function update_datasiswa($tabel,$data,$where)
	{
		return $this->db->update($tabel,$data,$where);
	}

	function delete_datasiswa($tabel,$where)
	{
		return $this->db->delete($tabel,$where);
	}

}

/* End of file M_entry.php */
/* Location: ./application/models/M_entry.php */
