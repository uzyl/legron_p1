<script type="text/javascript">
  $.fn.DataTable.ext.pager.simple_numbers_no_ellipses = function(page, pages){
   var numbers = [];
   var buttons = $.fn.DataTable.ext.pager.numbers_length;
   var half = Math.floor( buttons / 2 );
 
   var _range = function ( len, start ){
      var end;
    
      if ( typeof start === "undefined" ){
         start = 0;
         end = len;
 
      } else {
         end = start;
         start = len;
      }
 
      var out = [];
      for ( var i = start ; i < end; i++ ){ out.push(i); }
    
      return out;
   };
     
 
   if ( pages <= buttons ) {
      numbers = _range( 0, pages );
 
   } else if ( page <= half ) {
      numbers = _range( 0, buttons);
 
   } else if ( page >= pages - 1 - half ) {
      numbers = _range( pages - buttons, pages );
 
   } else {
      numbers = _range( page - half, page + half + 1);
   }
 
   numbers.DT_el = 'span';
 
   return [ 'previous', 'next' ];
};

</script>
<?php 
$data=$this->session->flashdata('sukses');
if($data!=""){ ?>
<div class="alert alert-success"><strong>Sukses! </strong> <?=$data;?></div>
<?php } ?>
<?php 
$data2=$this->session->flashdata('error');
if($data2!=""){ ?>
<div class="alert alert-danger"><strong> Error! </strong> <?=$data2;?></div>
<?php } ?>


<div class="panel panel-primary">
  <div class="panel-heading">
    <h5 class="panel-title">
      <i class="icon-sphere"></i> SOAL
    </h5>
  </div>

  <div class="panel-body">
  <!-- <form action="<?php echo site_url('aksespeserta/SPeserta/add'); ?>" id="checl" method="post">  -->
  <!-- <input type="hidden" id="counts" name="counts" value="<?php echo count($all);?>"> -->
  <form method="post">
   <table id="example"  class="table hover order-column" cellspacing="0" width="100%">
    <thead style="font-size:15px">
      <tr>
        <th>Nomor</th>
        <th>Deskripsi</th>
        <th>Soal</th>
        <th>Jawab</th>
      </tr>
      </thead>
    <tbody style="font-size:14px">
      <?php $no=-1; foreach($all as $row): $no++; ?>
      
     
      <tr class="checklisttr">
        <td><b><?php echo $no+1; ?></b></td>                         
        <td><b class="soal"><?php echo $row['deskripsi']; ?></b></td>
        <td><b class="soal"><?php echo $row['soal']; ?></b></td>
        <td><input type="radio" name="data[<?= $no ?>]" <?php echo ($hasill[$no]->jawaban=='A_'.$row['Jwaban'][0])?'checked':'' ?> value="<?php echo 'A_'; echo $row['Jwaban'][0];?>" disabled><?php echo " A. "; echo $row['Jwaban'][0]; ?></td>
      </tr>
      <tr>
        <td class="never"></td>
        <td class="never"></td>
        <td class="never"></td>
        <td><input type="radio" name="data[<?= $no ?>]" <?php echo ($hasill[$no]->jawaban=='B_'.$row['Jwaban'][1])?'checked':'' ?> value="<?php echo 'B_'; echo $row['Jwaban'][1];?>" disabled><?php echo " B. "; echo $row['Jwaban'][1]; ?></td>
      </tr>
      <tr>
        <td></td>
        <td></td>
        <td></td>
        <td><input type="radio" name="data[<?= $no ?>]" <?php echo ($hasill[$no]->jawaban=='C_'.$row['Jwaban'][2])?'checked':'' ?> value="<?php echo 'C_'; echo $row['Jwaban'][2];?>" disabled><?php echo " C. "; echo $row['Jwaban'][2]; ?></td>
      </tr>
      <tr>
        <td></td>
        <td></td>
        <td></td>
        <td><input type="radio" name="data[<?= $no ?>]" <?php echo ($hasill[$no]->jawaban=='D_'.$row['Jwaban'][3])?'checked':'' ?> value="<?php echo 'D_'; echo $row['Jwaban'][3];?>" disabled><?php echo " D. "; echo $row['Jwaban'][3]; ?></td>
      
      <tr>
        <td></td>
        <td></td>
        <td></td>
        <td><input type="radio" name="data[<?= $no ?>]" <?php echo ($hasill[$no]->jawaban=='E_'.$row['Jwaban'][4])?'checked':'' ?> value="<?php echo 'E_'; echo $row['Jwaban'][4];?>" disabled><?php echo " E. "; echo $row['Jwaban'][4]; ?></td>
      </tr>
      <tr>
        <td></td><td></td><td></td><td></td>
      </tr>
<!-- </tr> -->
    <?php endforeach; ?>
    
  </tbody>
</table>
<!-- <tr>
      <td colspan="5">
        <div class="pull-right">
              <button type="submit" id="quiz" name="quiz" class="btn btn-warning"><i class="icon-pencil5"></i> Done</button>
        </div>
      </td>
    </tr> -->
</form>



</div>
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">
<style type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.2/jquery.toast.css"></style>
 <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.2/jquery.toast.js"></script>

<script type="text/javascript">
   $('#example').DataTable({
   "pagingType": "simple_numbers_no_ellipses",
       "searching": false,
       "sort": false,
       "bInfo": false,
       "bLengthChange": false,
       "pageLength": 6
   });
</script>
