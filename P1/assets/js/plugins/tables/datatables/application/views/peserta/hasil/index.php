<?php 
$data=$this->session->flashdata('sukses');
if($data!=""){ ?>
<div class="alert alert-success"><strong>Sukses! </strong> <?=$data;?></div>
<?php } ?>
<?php 
$data2=$this->session->flashdata('error');
if($data2!=""){ ?>
<div class="alert alert-danger"><strong> Error! </strong> <?=$data2;?></div>
<?php 
} 
$user=$this->session->userdata('user'); 
?>

<div class="panel panel-primary">
  <div class="panel-heading">
    <h5 class="panel-title"><i class="icon-people"></i> Hasil Ujian
      <!-- <input type='submit' class="pull-right btn btn-danger" id="proses" name="proses" value="Proses Hasil">  -->
      <input type="hidden" id="user" name="user" value="<?php echo $user; ?>"> 

    </h5>
    
  </div>
  <div class="panel-body">
    <div class="well well-sm">

      <div class="row">
        <div class="col-md-4">


          <div class="form-group"> 
            <span class="input-group-addon"><i class="fa fa-user"> Nama Lengkap</i></span>
            <input type="text" id="namalk" name="namalk" value="<?php echo $bio['nama'];?>" autocomplete="off" placeholder="Nama" class="form-control" required readonly>
          </div>
        </div>
        

        <div class="col-md-4">
          <div class="form-group">
            <span class="input-group-addon"><i class="fa fa-calendar"> Tanggal Lahir</i></span>
            <input type="text" id="tgl_lahir" name="tgl_lahir" value="<?php echo $bio['tgl_lahir'];?>" autocomplete="off" placeholder="Tgl Lahir" class="form-control" required readonly>
          </div>
        </div>
        <div class="col-md-4">

          <div class="form-group">
            <span class="input-group-addon"><i class="fa fa-users"> Jenis Kelamin</i></span>
            <input type="text" name="jk" id="jk" placeholder="Jenis Kelamin" value="<?php echo $bio['jenis_kelamin'];?>" class="form-control" readonly required>
          </div>

        </div> 
      </div>
      <div class="row">
        <div class="col-md-4">

          <div class="form-group">
            <span class="input-group-addon"><i class="fa fa-phone"> Hp</i> </span>
            <input type="text" name="hp" id="hp" value="<?php echo $bio['hp'];?>" placeholder="No Hp" class="form-control" readonly required>
          </div>

        </div> 
        <div class="col-md-4">
          <div class="form-group">
            <span class="input-group-addon"><i class="fa fa-envelope"> Email</i></span>
            <input type="email" name="email" id="email" value="<?php echo $bio['email'];?>" placeholder="Email" class="form-control" readonly required>
          </div>
        </div>


        <div class="col-md-4">
          <div class="form-group">
            <span class="input-group-addon"><i class="fa fa-road"> Alamat</i></span>
            <textarea name="alamat" id="alamat" value="<?php echo $bio['alamat'];?>" placeholder="Alamat" class="form-control" required readonly><?php echo $bio['alamat'];?></textarea>
          </div>
        </div>

      </div>
      <br>
      

     <center><b><h3>NILAI HASIL UJIAN</h3></b></center>
     <hr>
     <center><b><h1><?php echo $hasil['nilai'];?></h1></b></center>
     <center><h3>PREDIKAT YANG DI DAPATKAN </h3><b><h1> <?php echo $hasil['predikat'];?></h1></b></center>
     <center><b><h1>TERIMAKASI TELAH MELAKUKAN UJIAN CBT</h1></b></center>
     
    </div>
