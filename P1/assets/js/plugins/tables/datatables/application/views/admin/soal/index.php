<?php 
$data=$this->session->flashdata('sukses');
if($data!=""){ ?>
<div class="alert alert-success"><strong>Sukses! </strong> <?=$data;?></div>
<?php } ?>
<?php 
$data2=$this->session->flashdata('error');
if($data2!=""){ ?>
<div class="alert alert-danger"><strong> Error! </strong> <?=$data2;?></div>
<?php } ?>
<div class="panel panel-primary">
  <div class="panel-heading" style="    color: #fff;
    background-color: #ff5722;
    border-color: #ff5722;">
    <h5 class="panel-title"><i class="icon-sphere"></i> SOAL
      <button type="button" class="btn btn-warning btn-sm pull-right" data-toggle="modal" data-target="#modal_theme_primary"><i class="icon-file-plus"></i>Tambah</button>

    </h5>
  </div>
  <div class="panel-body">
   <table id="example" class="table table-bordered">
    <thead>
      <tr>
        <th>Nomor</th>
        <th>Deskripsi</th>
        <th>Soal</th>
        <th>Pilihan Jawaban</th>
        <th>Data Jawaban</th>
        <th>Date Created</th>
        <th><center>Opsi</center></th>
      </tr>
    </thead>
    <tbody>
      <?php $no=0; foreach($all as $row): $no++; ?>
      <tr>
        <td><?php echo $no; ?></td>
        <td><?php echo substr($row->deskripsi,0,25); ?></td>
        <td><?php echo substr($row->soal,0,25); ?></td>
        <td><?php echo $row->jawaban; ?></td>
        <td><?php echo $row->setjawaban; ?></td>
        <td><?php echo $row->date_created; ?></td>
        <td>
          <center>
            <a data-toggle="modal" data-target="#modal_edit<?=$row->id_soal;?>" class="btn btn-info btn-xs" data-popup="tooltip" data-original-title="Edit Data" data-placement="top"><i class="icon-pencil5"></i></a>
            <a href="<?php echo site_url('Soal/hapus/'.$row->soal); ?>" onclick="return confirm('Apakah Anda Ingin Menghapus Data Judul ini, Maka Data Akan Terhapus atas Judul tersebut');" class="btn btn-danger btn-xs tooltips" data-popup="tooltip" data-original-title="Hapus Data" data-placement="top"><i class="icon-x"></i></a>
          </center>
        </td>
        
      </tr>
    <?php endforeach; ?>
  </tbody>
</table>
</div>
<style type="text/css">
div.dt-buttons{
position:relative;
float:right;
}
</style>
<script type="text/javascript">
    $('#example').dataTable({
       "paging": true,
       "searching": true,
        "scrollX": true,
          dom: 'Bfrtip',
          lengthMenu: [
            [ 10, 25, 50, -1 ],
            [ '10 rows', '25 rows', '50 rows', 'Show all' ]
        ],
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print','pageLength'
        ]
    });
</script>




<div id="modal_theme_primary" class="modal fade">
  <div class="modal-dialog">
    <form action="<?php echo site_url('Soal/tambah'); ?>" method="post">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h6 class="modal-title"><strong>Tambah Data</strong></h6>
        </div>
        <div class="modal-body">
          <div class="panel panel-primary">
          <div class="panel-heading">
            <h5 class="panel-title blue"><i class="fa fa-question-circle"></i> Set Soal / Bagian</h5>
          </div>
        </div>
          <br>
          <div class="row">
           <div class="col-md-12">
              <div class="form-group">
                <span class="input-group-addon"><i class="fa fa-question-circle"></i>  Jumlah Soal</span>
               <input type="number" name="set" id="set" autocomplete="off" required placeholder="Masukkan Jumlah Soal" class="form-control" >
              </div>
            </div>
          </div>  
          <br>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-warning"><i class="icon-checkmark-circle2"></i> Set</button>
        </div>
        </div>
      </form>
    </div>
  </div>



<?php $no=0; foreach($all as $row): $no++; ?>
  <div class="row">
    <div id="modal_edit<?=$row->id_soal;?>" class="modal fade">
      <div class="modal-dialog">
        <form action="<?php echo site_url('Soal/edit/'.$row->id_soal.'/'.$row->soal); ?>" method="post">
          <div class="modal-content">
            <div class="modal-header bg-primary">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h6 class="modal-title"><strong>Edit Data</strong></h6>
            </div>
            <div class="modal-body">
            <div class="row">
             <input type="text" nama="ceksoal" id="ceksoal" value="<?=$row->soal;?>" hidden>
             <input type="hidden" nama="cekdeskripsi" id="cekdeskripsi" value="<?=$row->deskripsi;?>">
          <div class="row">  
            <div class="col-md-12">
              <div class="form-group">
                <span class="input-group-addon"><i class="fa fa-question-circle"></i>  Deskripsi</span>
               <textarea name="deskripsi" autocomplete="off" required placeholder="Masukkan Soal" class="form-control"><?=$row->deskripsi;?></textarea>
              </div>
            </div>
          </div>  

          <div class="row">  
            <div class="col-md-12">
              <div class="form-group">
                <span class="input-group-addon"><i class="fa fa-question-circle"></i>  Soal</span>
               <input type="text" name="soal" id="soal" value="<?=$row->soal;?>" autocomplete="off" required placeholder="Masukkan Soal" class="form-control" >
              </div>
            </div>
          </div>  

        <div class="row">  
          <br>
            <div class="col-md-12">
              <div class="form-group">
                <span class="input-group-addon"><i class="fa fa-question-circle"></i>  Options Jawaban</span>
               <textarea name="jawaban" autocomplete="off" required placeholder="Masukkan Soal" class="form-control" ><?=$row->jawaban;?></textarea>
              </div>
            </div>
          </div>  
           <div class="row">  
          <br>
            <div class="col-md-12">
              <div class="form-group">
                <span class="input-group-addon"><i class="fa fa-question-circle"></i>  Set Jawaban</span>
                <select name="setjwb" id="setjwb" class="setJwb form-control">
                  <option <?php if ($row->setjawaban == 'A'){echo "selected";} ?> value="A">A</option>
                  <option <?php if ($row->setjawaban == 'B'){echo "selected";} ?> value="B">B</option>
                  <option <?php if ($row->setjawaban == 'C'){echo "selected";} ?> value="C">C</option>
                  <option <?php if ($row->setjawaban == 'D'){echo "selected";} ?> value="D">D</option>
                  <option <?php if ($row->setjawaban == 'E'){echo "selected";} ?> value="E">E</option>
                </select> 
              </div>
            </div>
          </div>  
          <br>

          
           
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-warning"><i class="icon-pencil5"></i> Edit</button>
            </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  <?php endforeach; ?>
