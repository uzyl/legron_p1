<?php
$data=$this->session->flashdata('sukses');
if($data!=""){ ?>
<div class="alert alert-success"><strong>Sukses! </strong> <?=$data;?></div>
<?php } ?>
<?php
$data2=$this->session->flashdata('error');
if($data2!=""){ ?>
<div class="alert alert-danger"><strong> Error! </strong> <?=$data2;?></div>
<?php } ?>
<div class="panel panel-primary">
  <div class="panel-heading" style="    color: #fff;
    background-color: #ff5722;
    border-color: #ff5722;">
    <h5 class="panel-title"><i class="icon-collaboration"></i> Manajemen Akses</h5>
  </div>
  <div class="panel-body">
  <div class="well well-sm">
    <form action="<?php echo site_url('Admin/add'); ?>" method="post">
    <div class="row">
      <div class="col-md-4">
        <center>
          <div class="form-group">
            <input type="text" name="nama" autocomplete="off" placeholder="Nama" class="form-control" required>
          </div>
        </center>
      </div>
      <div class="col-md-4">
        <center>
          <div class="form-group">
            <input type="text" name="username" autocomplete="off" placeholder="Username" class="form-control" required>
          </div>
        </center>
      </div>
      <div class="col-md-4">
        <center>
          <div class="form-group">
            <input type="password" name="password" autocomplete="off" placeholder="Password" class="form-control" required>
          </div>
        </center>
      </div>
    </div>
    <br>
    <div class="row">
      <div class="col-md-4">
        <center>
           <select name="akses" class="select-clear" required>
              <!-- <option value="">-- Hak Akses --</option> -->
              <option value="1">Superadmin</option>
              <!-- <option value="2">Admin</option>         -->
          </select>
        </center>
      </div>
      <div class="col-md-3">
        <center>
          <button type="submit" class="btn btn-warning btn-sm"><i class="icon-file-plus"></i> Tambah </button>
        </center>
      </div>
    </div>
      </form>
  </div>
  <br>
   <table class="table table-bordered datatable">
      <thead>
          <tr>
              <th>No</th>
              <th>Nama</th>
              <th>Username</th>
              <th>Akses</th>
              <th>Akses</th>
              <th class="never">Opsi</th>
          </tr>
      </thead>

        <?php $no=0; foreach($all as $row): $no++;?>
            <tr>
                <td></td>
                <td><?php echo $row->nama; ?></td>
                <td><?php echo $row->username; ?></td>
                <td><?php echo $no; ?></td>


                  <td><?php if($row->akses==1){echo "Superadmin";}else{echo "Admin";}; ?></td>
                 <td>
            <a data-toggle="modal" data-target="#modal_edit<?=$row->id_admin;?>" class="btn btn-info btn-xs" data-popup="tooltip" data-original-title="Edit Data" data-placement="top"><i class="icon-pencil5"></i></a>
           <a href="<?php echo site_url('Admin/delete/'.$row->id_admin); ?>" onclick="return confirm('Apakah Anda Ingin Menghapus Data Ini');" class="btn btn-danger btn-xs tooltips" data-popup="tooltip" data-original-title="Hapus Data" data-placement="top"><i class="icon-x"></i></a>
                </td>

            </tr>
      <?php  endforeach; ?>
    </table>
  </div>
</div>


<!--  -->
<?php $no=0; foreach($all as $row): $no++; ?>
  <div class="row">
    <div id="modal_edit<?=$row->id_admin;?>" class="modal fade">
      <div class="modal-dialog">
        <form action="<?php echo site_url('Admin/edit/'.$row->id_admin); ?>" method="post">
          <div class="modal-content">
            <div class="modal-header bg-orange">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h6 class="modal-title"><strong>Edit Data</strong></h6>
            </div>
            <div class="modal-body">

        <div class="row">
          <div class="col-xs-6 col-sm-4">
            <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-user"></i></span>
              <input type="text" name="nama" id="nama" class="form-control" value="<?php echo $row->nama; ?>" placeholder="nama" required>
            </div>
          </div>
          <div class="col-xs-6 col-sm-4">
            <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-address-card"></i></span>
              <input type="text" value="<?php echo $row->username; ?>" class="form-control" style="color:black; height:35px;" id="username" name="username" placeholder="Username" required>
            </div>
          </div>
          <div class="col-xs-6 col-sm-4">
            <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-list"></i></span>
              <input type="password" name="password" value="<?php echo $row->password; ?>" id="password" class="form-control" placeholder="password" required>
            </div>
          </div>
        </div></br>
          <div class="row">

            <div class="col-md-12">
              <div class="form-group">
                <span class="input-group-addon"><i class="fa fa-question-circle"></i>  Akses</span>
                <select name="akses" class="select-clear" required>
                  <!-- <option value="">-- Hak Akses --</option> -->
                  <option <?php if ($row->akses == 1){echo "selected";} ?> value="1">Superadmin</option>
                  <!-- <option <?php if ($row->akses == 2){echo "selected";} ?> value="2">Admin</option>         -->
                </select>
              </div>
            </div>
          </div>
          <br>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-warning"><i class="icon-pencil5"></i> Edit</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  <?php endforeach; ?>
