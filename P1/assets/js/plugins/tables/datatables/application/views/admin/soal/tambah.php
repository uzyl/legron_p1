  <?php 
  $data=$this->session->flashdata('sukses');
  if($data!=""){ ?>
  <div class="alert alert-success"><strong>Sukses! </strong> <?=$data;?></div>
  <?php } ?>
  <?php 
  $data2=$this->session->flashdata('error');
  if($data2!=""){ ?>
  <div class="alert alert-danger"><strong> Error! </strong> <?=$data2;?></div>
  <?php } ?>
  <div class="panel panel-primary">
    <div class="panel-heading" style="    color: #fff;
    background-color: #ff5722;
    border-color: #ff5722;">
      <h5 class="panel-title blue"><i class="fa fa-question-circle"></i> KESELURUHAN SOAL HARUS TERDAPAT <?php echo $sets; ?> SOAL</h5>
    </div>
    <div class="panel-body">


     <form method="post" id="pst" name="pst">
      <input type="hidden" id="set" name="set" value="<?php echo $sets; ?>">
       <div class="form-group">
         <div class="row">
          <div class="col-xs-2 col-sm-12">
            <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-list"> Deskripsi</i></span>
              <textarea name="deskripsi" id="deskripsi" placeholder="deskripsi" class="form-control" required></textarea>
            </div>
          </div>

        </div>
        <br>
       <!--  <div class="row">
          <div class="col-xs-6 col-sm-2">
           <div class="input-group">
            <button id="removeChoice" name="removeChoice" class="btn btn-danger btn-filter fa fa-click">Delete Coloum</button>
          </div>
        </div>
         <div class="col-xs-6 col-sm-2">
            <div class="input-group">
              <input type="number" class="form-control" id="del" name="del" placholder="Input No Soal">  
            </div>
          </div>
      </div></br> -->
      <div name="judul" class="judul"></div>
      </div>
      <br>
      
      <div class="modal-footer">
        <button type="reset" class="btn btn-default" data-dismiss="modal">Reset</button>
        <button type="submit" class="btn btn-warning"><i class="icon-pencil5"></i> Tambah</button>
      </div>
    </form>
  </div>
  <script type="text/javascript">
  // console.log($('#set').val())
  var choices = parseInt($('#set').val())

            var html = '';
                var i;
                for(i=0; i<choices; i++){
                    html +='<div class="row">',
                    html +='<div class="col-xs-12 col-md-8>',
                    html +='<div class="input-group">',
                    html +='<span class="input-group-addon" style="background:blue;color:white"><i class="fa fa-question-circle"></i>  Soal'+ (i+1) +'</span>',
                    html +='<input type="text" name="Question'+i+'" id="Question'+i+'" class="Question form-control" placeholder="Question" required>',
                    html +='</div></div></div></br>',

                    html +='<div class="row">',
                      html +='<div class="col-xs-4 col-md-4">',
                      html +='<div class="input-group">',
                      html +='<span class="input-group-addon">A</span>',
                      html +='<textarea name="A_jawaban'+i+'" id="A_jawaban'+i+'" class="A_jawaban form-control" placeholder="Option" required></textarea>',
                      html +='</div></div>'
                        html +='<div class="col-xs-4 col-md-4">',
                        html +='<div class="input-group">',
                        html +='<span class="input-group-addon">  B</span>',
                        html +='<textarea name="B_jawaban'+i+'" id="B_jawaban'+i+'" class="B_jawaban form-control" placeholder="Option" required></textarea>',
                        html +='</div></div>',
                          html +='<div class="col-xs-4 col-md-4">',
                          html +='<div class="input-group">',
                          html +='<span class="input-group-addon">  C</span>',
                          html +='<textarea name="C_jawaban'+i+'" id="C_jawaban'+i+'" class="C_jawaban form-control" placeholder="Option" required></textarea>',
                    html +='</div></div></div><br>';

                      html +='<div class="row">',
                        html +='<div class="col-xs-4 col-md-6">',
                        html +='<div class="input-group">',
                        html +='<span class="input-group-addon">  D</span>',
                        html +='<textarea name="D_jawaban'+i+'" id="D_jawaban'+i+'" class="D_jawaban form-control" placeholder="Option" required></textarea>',
                        html +='</div></div>'
                          html +='<div class="col-xs-4 col-md-6">',
                          html +='<div class="input-group">',
                          html +='<span class="input-group-addon">  E</span>',
                          html +='<textarea name="E_jawaban'+i+'" id="E_jawaban'+i+'" class="E_jawaban form-control" placeholder="Option" required></textarea>',
                      html +='</div></div></div><br>',
                         html +='<div class="row">',
                          html +='<div class="col-xs-12 col-md-12">',
                          html +='<div class="input-group">',
                          html +='<span class="input-group-addon">  Set Jawaban</span>',
                         html +='<select name="setJwb'+i+'" id="setJwb'+i+'" class="setJwb form-control"><option value="A">A</option><option value="B">B</option><option value="C">C</option><option value="D">D</option><option value="E">E</option></select>',
                         html +='</div></div></div><br>';
                   }
                $('.judul').html(html);

   // $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
   //      e.preventDefault(); $(this).parent('div').remove(); x--;
   //  })
   $('#removeChoice').on('click',function(event){
    var hasil = $('.Question').serializeArray()
    removeValFromIndex = [$('#del').val()];    

      for (var i = removeValFromIndex.length -1; i >= 0; i--)
        // event.preventDefault();
       hasil.splice(removeValFromIndex[i],1);
        var html = '';
                    var i;
                for(i=0; i<hasil.length; i++){
                        html +='<div class="row">',
                    html +='<div class="col-xs-12 col-md-8>',
                    html +='<div class="input-group">',
                    html +='<span class="input-group-addon" style="background:blue;color:white"><i class="fa fa-question-circle"></i>  Soal'+ (i+1) +'</span>',
                    html +='<input type="text" name="Question'+i+'" id="Question'+i+'" class="Question form-control" placeholder="Question" required>',
                    html +='</div></div></div></br>',

                    html +='<div class="row">',
                      html +='<div class="col-xs-4 col-md-4">',
                      html +='<div class="input-group">',
                      html +='<span class="input-group-addon">A</span>',
                      html +='<textarea name="A_jawaban'+i+'" id="A_jawaban'+i+'" class="A_jawaban form-control" placeholder="Option" required></textarea>',
                      html +='</div></div>'
                        html +='<div class="col-xs-4 col-md-4">',
                        html +='<div class="input-group">',
                        html +='<span class="input-group-addon">  B</span>',
                        html +='<textarea name="B_jawaban'+i+'" id="B_jawaban'+i+'" class="B_jawaban form-control" placeholder="Option" required></textarea>',
                        html +='</div></div>',
                          html +='<div class="col-xs-4 col-md-4">',
                          html +='<div class="input-group">',
                          html +='<span class="input-group-addon">  C</span>',
                          html +='<textarea name="C_jawaban'+i+'" id="C_jawaban'+i+'" class="C_jawaban form-control" placeholder="Option" required></textarea>',
                    html +='</div></div></div><br>';

                      html +='<div class="row">',
                        html +='<div class="col-xs-4 col-md-6">',
                        html +='<div class="input-group">',
                        html +='<span class="input-group-addon">  D</span>',
                        html +='<textarea name="D_jawaban'+i+'" id="D_jawaban'+i+'" class="D_jawaban form-control" placeholder="Option" required></textarea>',
                        html +='</div></div>'
                          html +='<div class="col-xs-4 col-md-6">',
                          html +='<div class="input-group">',
                          html +='<span class="input-group-addon">  E</span>',
                          html +='<textarea name="E_jawaban'+i+'" id="E_jawaban'+i+'" class="E_jawaban form-control" placeholder="Option" required></textarea>',
                      html +='</div></div></div><br>',
                         html +='<div class="row">',
                          html +='<div class="col-xs-12 col-md-12">',
                          html +='<div class="input-group">',
                          html +='<span class="input-group-addon">  Set Jawaban</span>',
                          html +='<select name="setJwb'+i+'" id="setJwb'+i+'" class="setJwb form-control"><option value="A">A</option><option value="B">B</option><option value="C">C</option><option value="D">D</option><option value="E">E</option></select>',
                        html +='</div></div></div><br>';
                   }
                $('.judul').html(html);
  })
    
      $('#pst').submit(function(event){
           
            var hasil = $('.Question').serializeArray()
            console.log(hasil)

            var A_jawaban = $('.A_jawaban').serializeArray()
            // console.log(A_jawaban)

            var B_jawaban = $('.B_jawaban').serializeArray()
            // console.log(B_jawaban)

            var C_jawaban = $('.C_jawaban').serializeArray()
            // console.log(C_jawaban)

            var D_jawaban = $('.D_jawaban').serializeArray()
            console.log(D_jawaban)

            var E_jawaban = $('.E_jawaban').serializeArray()

            var Set = $('.setJwb').serializeArray()
            console.log(Set)
            var array = []
            for (var i in hasil){
              array.push(
                {
                  "IDsoal":hasil[i].name,
                  "soal":hasil[i].value,
                  "SoalsJwbID":Set[i].name,
                  "SoalsJwb":Set[i].value,
                  "jawaban":[A_jawaban[i],B_jawaban[i],C_jawaban[i],D_jawaban[i],E_jawaban[i]]
                }
              )
              
            }
            console.log(array);
            var deskrip = $('#deskripsi').val()
           

          $.ajax({
                  url : "<?php echo base_url();?>Soal/add",
                  method : "POST",
                  data : {has:array,desk:deskrip},
                  success:function(err,sukses){
                    alert('Sukses Disimpan');
                    window.location.href = "<?php echo site_url('Soal'); ?>";
                  },error:function(err){
                    alert('Terjadi Kesalahan');
                    window.location.href = "<?php echo site_url('Soal'); ?>";
                  }
          })
                          
      })
    </script>

