<?php 
$data=$this->session->flashdata('sukses');
if($data!=""){ ?>
<div class="alert alert-success"><strong>Sukses! </strong> <?=$data;?></div>
<?php } ?>
<?php 
$data2=$this->session->flashdata('error');
if($data2!=""){ ?>
<div class="alert alert-danger"><strong> Error! </strong> <?=$data2;?></div>
<?php } ?>
<div class="panel panel-primary">
  <div class="panel-heading" style="    color: #fff;
    background-color: #ff5722;
    border-color: #ff5722;">
    <h5 class="panel-title"><i class="icon-people"></i> Hasil Keseluruhan Data Ujian</h5>
  </div>
  <div class="panel-body">
  <div class="well well-sm">
<div class="well well-sm">
   <table id="example" class="table table-bordered">

      <thead>
          <tr>
              <th>Aksi</th>
              <th>No</th>
              <th>Nama</th>
              <th>Bulan Test</th>
              <th>Jenis Kelamin</th>
              <th>Tanggal Lahir</th>
              <th>Hp</th>      
              <th>Nilai Ujian</th>
              <th>Predikat</th>
          </tr>
      </thead>
      
        <?php $no=0; foreach($all as $row): $no++;?>      
            <tr>
                <td>
                 <a data-toggle="modal" data-target="#modal_edit<?=$row->id;?>" class="btn btn-info btn-xs" data-popup="tooltip" data-original-title="Edit Data" data-placement="top"><i class="icon-pencil5"></i></a>
                 <a href="<?php echo site_url('hasil/hapus/'.$row->id); ?>" onclick="return confirm('Apakah Anda Yakin Ingin Menghapus Data Ini?');" class="btn btn-danger btn-xs tooltips" data-popup="tooltip" data-original-title="Hapus Data" data-placement="top"><i class="icon-x"></i></a>
                </td>
                <td><?php echo $no; ?></td>
                <td><?php echo $row->nama; ?></td>
                <td><?php echo $row->date_created; ?></td>
                <td><?php echo $row->jenis_kelamin; ?></td>
                <td><?php echo $row->tgl_lahir; ?></td>
                <td><?php echo $row->hp; ?></td>
                <td><?php echo $row->nilai; ?></td>
                <td><?php echo $row->predikat; ?></td>
                
            </tr>
      
      <?php  endforeach; ?>

      
    </table>
  </div>
  <br>
  
  </div>
</div>
<style type="text/css">
div.dt-buttons{
position:relative;
float:right;
}
</style>
<script type="text/javascript">
    $('#example').dataTable({
       "paging": true,
       "searching": true,
        "scrollX": true,
          dom: 'Bfrtip',
          lengthMenu: [
            [ 10, 25, 50, -1 ],
            [ '10 rows', '25 rows', '50 rows', 'Show all' ]
        ],
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print','pageLength'
        ]
    });

 
</script>

<?php $no=0; foreach($all as $row): $no++; ?>
  <div class="row">
    <div id="modal_edit<?=$row->id;?>" class="modal fade">
      <div class="modal-dialog">
        <form action="<?php echo site_url('Hasil/edit/'.$row->id); ?>" method="post">
          <div class="modal-content">
            <div class="modal-header bg-primary">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h6 class="modal-title"><strong>Edit Data</strong></h6>
            </div>
            <div class="modal-body">
           
        <div class="row">
          <div class="col-xs-2 col-sm-6">
            <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-user"> Nama Lengkap</i></span>
              <input type="text" name="nama" id="nama" class="form-control" value="<?php echo $row->nama; ?>" placeholder="nama" readonly>
            </div>
          </div>
          <div class="col-xs-2 col-sm-6">
            <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-calendar"> Tanggal Lahir</i></span>
              <input type="date" value="<?php echo $row->tgl_lahir; ?>" class="form-control" style="color:black; height:35px;" id="tgl_lahir" name="tgl_lahir" placeholder="Username" readonly>
            </div>
          </div>
        </div></br>
        <div class="row">
          <div class="col-xs-2 col-sm-6">
            <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-phone"> Hp</i> </span>
              <input type="text" value="<?php echo $row->hp; ?>"  pattern="[0-9]*" minlength="11" maxlength="12" name="hp" id="hp" placeholder="No Hp" class="form-control" readonly>
          </div>
          </div>
          <div class="col-md-6">
               <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-user">Jenis Kelamin</i></span>
           <select name="jk" class="select-clear" readonly>
              <option value="">-- Jenis Kelamin --</option>
              <option <?php if ($row->jenis_kelamin == 'Laki-Laki'){echo "selected";} ?> value="Laki-Laki" readonly>Laki-Laki</option>
              <option <?php if ($row->jenis_kelamin == 'Perempuan'){echo "selected";} ?> value="Perempuan" readonly>Perempuan</option>
              
          </select>
        </div>
      </div>
        </div>
        <br>
         <div class="row">
          <div class="col-xs-2 col-sm-6">
            <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-star"> Nilai Ujian</i> </span>
              <input type="text" value="<?php echo $row->nilai; ?>"  minlength="1" maxlength="3" name="nilai" id="nilai" placeholder="Nilai Ujian" class="form-control" required>
          </div>
          </div>
     <div class="col-md-6">
               <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-star">Predikat</i></span>
           <select name="predikat" class="select-clear" required>
              <option value="">-- Pilih Predikat --</option>
              <option <?php if ($row->predikat == 'Kurang'){echo "selected";} ?> value="Kurang">Kurang</option>
              <option <?php if ($row->predikat == 'Cukup'){echo "selected";} ?> value="Cukup">Cukup</option>
              <option <?php if ($row->predikat == 'Baik'){echo "selected";} ?> value="Baik">Baik</option>
              <option <?php if ($row->predikat == 'Sangat Baik'){echo "selected";} ?> value="Sangat Baik">Sangat Baik</option>
              
          </select>
        </div>
      </div>
            </div>
            <br>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-warning"><i class="icon-pencil5"></i> Edit</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  <?php endforeach; ?>