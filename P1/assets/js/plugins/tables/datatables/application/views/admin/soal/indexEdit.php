<?php 
$data=$this->session->flashdata('sukses');
if($data!=""){ ?>
<div class="alert alert-success"><strong>Sukses! </strong> <?=$data;?></div>
<?php } ?>
<?php 
$data2=$this->session->flashdata('error');
if($data2!=""){ ?>
<div class="alert alert-danger"><strong> Error! </strong> <?=$data2;?></div>
<?php } ?>
<div class="panel panel-primary">
  <div class="panel-heading">
    <h5 class="panel-title blue"><i class="fa fa-question-circle"></i> KESELURUHAN SOAL HARUS TERDAPAT 40 SOAL</h5>
  </div>
  <div class="panel-body">

   <form action="<?php echo site_url('Soal/edit/'.$idnya); ?>" method="post">
             <div class="form-group">
            <label class='col-md-3'>Kategori</label>
            <div class='col-md-9'>
              <select name="id" class="form-control" required>
                <option value="">-- Pilih Kategori --</option>
                 <?php foreach($katGr as $data){?>
                    <option <?php if($idnya == $data->id_kat_soal){echo "selected";}?> value="<?=$data->id_kat_soal?>"><?= $data->kategori ?></option>
                 <?php }?>
              </select>
            </div>
          </div>
          <br><br>
          <div class="row">
             <?php $no=0; foreach($katGrById as $dataArr) : $no++;  ?>
            <div class="col-md-4">
              <div class="form-group">
                <span class="input-group-addon"><i class="fa fa-question-circle"></i>  Soal <?php echo $no; ?></span>
               <input type="text" name="data[<?= $no ?>]" value="<?=$dataArr->soal;?>" autocomplete="off" required placeholder="Masukkan Soal" class="form-control" >
                <input type="text" name="dataID[<?= $no ?>]" value="<?=$dataArr->id_soal;?>" hide="true">
              </div>
            </div>
             <?php endforeach; ?>
          </div>  
          <br>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-warning"><i class="icon-pencil5"></i> Edit</button>
            </div>
          </form>
</div>

