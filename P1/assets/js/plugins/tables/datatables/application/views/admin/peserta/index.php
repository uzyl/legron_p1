<?php 
$data=$this->session->flashdata('sukses');
if($data!=""){ ?>
<div class="alert alert-success"><strong>Sukses! </strong> <?=$data;?></div>
<?php } ?>
<?php 
$data2=$this->session->flashdata('error');
if($data2!=""){ ?>
<div class="alert alert-danger"><strong> Error! </strong> <?=$data2;?></div>
<?php } ?>
<div class="panel panel-primary">
  <div class="panel-heading" style="    color: #fff;
    background-color: #ff5722;
    border-color: #ff5722;">
    <h5 class="panel-title"><i class="icon-people"></i> Data Peserta</h5>
  </div>
  <div class="panel-body">
  <div class="well well-sm">
    <form action="<?php echo site_url('Peserta/add'); ?>" method="post">
    <div class="row">
       <div class="col-md-4">
        <center>
           <span class="input-group-addon"><i class="fa fa-user"> No Peserta</i></span>
            <input type="text" name="identitas" id="identitas" autocomplete="off" value="<?php echo getid('peserta','identity_peserta'); ?>" placeholder="NO PESERTS" class="form-control" required readonly>
          
        </center>
      </div>

      <div class="col-md-4">
        <center>
           <span class="input-group-addon"><i class="fa fa-user"> Nama Lengkap</i></span>
            <input type="text" name="nama" autocomplete="off" placeholder="Nama" class="form-control" required>
         
        </center>
      </div>
      
      <div class="col-md-4">
        <center>
           <span class="input-group-addon"><i class="fa fa-calendar"> Tanggal Lahir</i></span>
            <input type="date" name="tgl_lahir" autocomplete="off" placeholder="Tgl Lahir" class="form-control" required>
        
        </center>
      </div>
      
      
    </div>
    <br>
    <div class="row">
      <div class="col-md-4">
         <span class="input-group-addon"><i class="fa fa-envelope"> Email</i></span>
             <input type="email" name="email" id="email" placeholder="Email" class="form-control" required>
         
      </div>
      
      <div class="col-md-4">
         <span class="input-group-addon"><i class="fa fa-road"> Alamat</i></span>
             <textarea name="alamat" id="alamat" placeholder="Alamat" class="form-control" required></textarea>
          
      </div>
      
      
       <div class="col-md-4">
        
           <span class="input-group-addon"><i class="fa fa-phone">Hp</i></span>
             <input type="text"  pattern="[0-9]*" minlength="11" maxlength="12" name="hp" id="hp" placeholder="No Hp" class="form-control" required>
          
      </div>
    </div>
    <br>
    <div class="row">
            <div class="col-md-4">
           <select name="jk" class="select-clear" required>
              <option value="">-- Jenis Kelamin --</option>
              <option value="Laki-Laki">Laki-Laki</option>
              <option value="Perempuan">Perempuan</option>
              
          </select>
      </div>
     
      <div class="col-md-4">
          <button type="submit" class="btn btn-warning btn-sm"><i class="icon-file-plus"></i> Tambah </button>
      </div>
    </div>
      </form>
</div>
  <br>
   <table id="example" class="table table-bordered">
      <thead>
          <tr>
              <th>No</th>
              <!-- <th>NIS</th> -->
              <th>Nama</th>
              <th>Username</th>
              <th>Jenis Kelamin</th>
              <th>Tanggal Lahir</th>
              <th>Hp</th>      
              <th>Email</th>
              <th>Alamat</th>
              <th>Kode Identitas</th>
              <th>Opsi</th>    
          </tr>
      </thead>
      
        <?php $no=0; foreach($all as $row): $no++;?>      
            <tr>
                <td><?php echo $no; ?></td>
                <!-- <td><?php echo $row->nis; ?></td> -->
                <td><?php echo $row->nama; ?></td>
                <td><?php echo $row->username; ?></td>
                <td><?php echo $row->jenis_kelamin; ?></td>
                <td><?php echo $row->tgl_lahir; ?></td>
                <td><?php echo $row->hp; ?></td>
                <td><?php echo $row->email; ?></td>
                <td><?php echo $row->alamat; ?></td>
                <td><?php echo $row->identity_peserta; ?></td>
                <td>
                  <a href="<?php echo site_url('Peserta/EditPs/'.$row->id_peserta); ?>" class="btn btn-info btn-xs" data-popup="tooltip" data-original-title="Edit Data" data-placement="top"><i class="icon-pencil5"></i></a>
                 <a href="<?php echo site_url('Peserta/hapus/'.$row->id_peserta); ?>" onclick="return confirm('Apakah Anda Yakin Ingin Menghapus Data Ini Makan Account Akan Terhapus?');" class="btn btn-danger btn-xs tooltips" data-popup="tooltip" data-original-title="Hapus Data" data-placement="top"><i class="icon-x"></i></a>
                </td>

            </tr>
      
      <?php  endforeach; ?>

      
    </table>
  </div>
</div>
<style type="text/css">
div.dt-buttons{
position:relative;
float:right;
}
</style>
<script type="text/javascript">
    $('#example').dataTable({
       "paging": true,
       "searching": true,
        "scrollX": true,
          dom: 'Bfrtip',
          lengthMenu: [
            [ 10, 25, 50, -1 ],
            [ '10 rows', '25 rows', '50 rows', 'Show all' ]
        ],
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print','pageLength'
        ]
    });
</script>

<br>


<?php $no=0; foreach($all as $row): $no++; ?>
  <div class="row">
    <div id="modal_editAcc<?=$row->id_peserta;?>" class="modal fade">
      <div class="modal-dialog">
        <form action="<?php echo site_url('Peserta/editAcc/'.$row->id_peserta); ?>" method="post">
          <div class="modal-content">
            <div class="modal-header bg-primary">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h6 class="modal-title"><strong>Edit Account</strong></h6>
            </div>
            <div class="modal-body">
            
        <div class="row">
          <div class="col-xs-2 col-sm-6">
            <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-user"> Username</i></span>
              <input type="text" name="username" id="username" class="form-control" value="<?php echo $row->username; ?>" placeholder="nama" required>
            </div>
          </div>
          <div class="col-xs-2 col-sm-6">
            <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-briefcase"> Password</i></span>
              <input type="password" value="<?php echo $row->password; ?>" class="form-control" style="color:black; height:35px;" id="password" name="password" placeholder="Username" required>
            </div>
          </div>
        </div></br>
        </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-warning"><i class="icon-pencil5"></i> Edit</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  <?php endforeach; ?>


<?php $no=0; foreach($all as $row): $no++; ?>
  <div class="row">
    <div id="modal_edit<?=$row->id_peserta;?>" class="modal fade">
      <div class="modal-dialog">
        <form action="<?php echo site_url('Peserta/edit/'.$row->id_peserta); ?>" method="post">
          <div class="modal-content">
            <div class="modal-header bg-orange">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h6 class="modal-title"><strong>Edit Data</strong></h6>
            </div>
            <div class="modal-body">
           
       <!--  <div class="row">
          <div class="col-xs-2 col-sm-12">
            <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-user"> NIS</i></span>
              <input type="text" name="nis" id="nis" class="form-control" value="<?php echo $row->nis; ?>" placeholder="nama" required>
            </div>
          </div>
        </div></br>  -->
        <div class="row">
          <div class="col-xs-2 col-sm-6">
            <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-user"> Nama Lengkap</i></span>
              <input type="text" name="nama" id="nama" class="form-control" value="<?php echo $row->nama; ?>" placeholder="nama" required>
            </div>
          </div>
          <div class="col-xs-2 col-sm-6">
            <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-calendar"> Tanggal Lahir</i></span>
              <input type="date" value="<?php echo $row->tgl_lahir; ?>" class="form-control" style="color:black; height:35px;" id="tgl_lahir" name="tgl_lahir" placeholder="Username" required>
            </div>
          </div>
        </div></br>
        <div class="row">
          <div class="col-xs-2 col-sm-6">
            <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-phone"> Hp</i> </span>
              <input type="text" value="<?php echo $row->hp; ?>"  pattern="[0-9]*" minlength="11" maxlength="12" name="hp" id="hp" placeholder="No Hp" class="form-control" required>
          </div>
          </div>
          <div class="col-xs-2 col-sm-6">
            <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-envelope"> Email</i></span>
              <input type="email" value="<?php echo $row->email; ?>" name="email" id="email" placeholder="Email" class="form-control" required>
          </div>
          </div>
        </div><br>
        <div class="row">
          <div class="col-xs-2 col-sm-6">
            <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-road"> Alamat</i></span>
               <textarea name="alamat" id="alamat" placeholder="Alamat" class="form-control" required><?php echo $row->alamat; ?></textarea>
          </div>
          </div>
            <div class="col-md-6">
               <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-user">Jenis Kelamin</i></span>
           <select name="jk" class="select-clear" required>
              <option value="">-- Jenis Kelamin --</option>
              <option <?php if ($row->jenis_kelamin == 'Laki-Laki'){echo "selected";} ?> value="Laki-Laki">Laki-Laki</option>
              <option <?php if ($row->jenis_kelamin == 'Perempuan'){echo "selected";} ?> value="Perempuan">Perempuan</option>
              
          </select>
        </div>
      </div>
        </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-warning"><i class="icon-pencil5"></i> Edit</button>
            </div>
          </form>
          <hr>
          <form action="<?php echo site_url('Peserta/editAccc'); ?>" method="post">
          <div class="modal-content">
            <div class="modal-header bg-primary">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h6 class="modal-title"><strong>Edit Account</strong></h6>
            </div>
            <div class="modal-body">
            
        <div class="row">
         
                    <div class="form-group">
                      <label class='col-md-3'>Username</label>
                      <div class='col-md-9'><input value="<?php echo $row->username; ?>" type="text" name="username" placeholder="Masukkan Username" class="form-control" ></div>
                    </div>
                    <br>
                    
                    <input type="hidden" name="passlama" value="<?php echo $row->password; ?>">
                    <input type="hidden" name="userlama" value="<?php echo $row->username; ?>">
                    <div class="form-group">
                      <label class='col-md-3'>Password Lama</label>
                      <div class='col-md-9'><input type="password" name="password" autocomplete="off" required placeholder="Masukkan Password" class="form-control" ></div>
                    </div>
                    <br>
                    <div class="form-group">
                      <label class='col-md-3'>Password Baru</label>
                      <div class='col-md-9'><input type="password" name="password_baru" autocomplete="off" required placeholder="Masukkan Password Baru" class="form-control" ></div>
                    </div>
                    <br>
                    <div class="form-group">
                      <label class='col-md-3'>Konfirmasi Password Baru</label>
                      <div class='col-md-9'><input type="password" name="konf_baru" autocomplete="off" required placeholder="Masukkan Konfirmasi Password Baru" class="form-control" ></div>
                    </div>
                    <br>
                     <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-warning"><i class="icon-pencil5"></i> Edit</button>
            </div>  
        </div>
          </form>
        </div>
      </div>
    </div>
  <?php endforeach; ?>
