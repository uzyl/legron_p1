<script type="text/javascript">
  $.fn.DataTable.ext.pager.simple_numbers_no_ellipses = function(page, pages){
   var numbers = [];
   var buttons = $.fn.DataTable.ext.pager.numbers_length;
   var half = Math.floor( buttons / 2 );
 
   var _range = function ( len, start ){
      var end;
    
      if ( typeof start === "undefined" ){
         start = 0;
         end = len;
 
      } else {
         end = start;
         start = len;
      }
 
      var out = [];
      for ( var i = start ; i < end; i++ ){ out.push(i); }
    
      return out;
   };
     
 
   if ( pages <= buttons ) {
      numbers = _range( 0, pages );
 
   } else if ( page <= half ) {
      numbers = _range( 0, buttons);
 
   } else if ( page >= pages - 1 - half ) {
      numbers = _range( pages - buttons, pages );
 
   } else {
      numbers = _range( page - half, page + half + 1);
   }
 
   numbers.DT_el = 'span';
 
   return [ 'previous', 'next' ];
};

</script>
<?php 
$data=$this->session->flashdata('sukses');
if($data!=""){ ?>
<div class="alert alert-success"><strong>Sukses! </strong> <?=$data;?></div>
<?php } ?>
<?php 
$data2=$this->session->flashdata('error');
if($data2!=""){ ?>
<div class="alert alert-danger"><strong> Error! </strong> <?=$data2;?></div>
<?php } ?>

<div id="err"></div>
<div class="panel panel-primary">
  <div class="panel-heading">
    <h5 class="panel-title">
      <i class="icon-sphere"></i> SOAL
    
    <div class="pull-right" style="font-weight: bold; font-size: 18px;" id="quiz-time-left"></div>
    </h5>
  <script type="text/javascript">
var max_time = 55;
var c_seconds  = 511;
var total_seconds =60*max_time;
max_time = parseInt(total_seconds/60);
c_seconds = parseInt(total_seconds%60);
document.getElementById("quiz-time-left").innerHTML='Time Left: ' + max_time + ' minutes ' + c_seconds + ' seconds';
function init(){
document.getElementById("quiz-time-left").innerHTML='Time Left: ' + max_time + ' minutes ' + c_seconds + ' seconds';
setTimeout("CheckTime()",999);
}
function CheckTime(){
document.getElementById("quiz-time-left").innerHTML='Time Left: ' + max_time + ' minutes ' + c_seconds + ' seconds' ;
if(total_seconds <=0){
setTimeout('document.quiz.submit()',1);
    
    } else
    {
total_seconds = total_seconds -1;
max_time = parseInt(total_seconds/60);
c_seconds = parseInt(total_seconds%60);
setTimeout("CheckTime()",999);
}

}
init();
 </script>
 <script type="text/javascript">
function finishpage()
{
alert("unload event detected!");
confirm('Apakah Anda Yakin Ingin Melakukan Test Dan Test Hanya Dapat Dilakukan Satu Kali');
document.quiz.submit();


}
</script>
  </div>

  <div class="panel-body">
  <!-- <form action="<?php echo site_url('aksespeserta/SPeserta/add'); ?>" id="checl" method="post">  -->
  <input type="hidden" id="counts" name="counts" value="<?php echo count($all);?>">
  <form id="quiz" name="quiz" method="post">
   <table id="example"  class="table hover order-column" cellspacing="0" width="100%">
    <thead style="font-size:15px">
      <tr>
        <th>Nomor</th>
        <th width="255px">Deskripsi</th>
        <th width="355px">Soal</th>
        <th>Jawab</th>
      </tr>
      </thead>
    <tbody style="font-size:14px">
      <?php $no=-1; foreach($all as $row): $no++;  ?>
      <tr class="checklisttr">
        <td><b><?php echo $no+1; ?></b></td>                         
        <td><b class="soal"><p align="justify"><?php echo $row['deskripsi']; ?></p></b></td>
        <td><b class="soal"><p align="justify"><?php echo $row['soal']; ?></p></b></td>
        <td><input type="radio" name="data[<?= $no ?>]" value="<?php echo 'A_'; echo $row['Jwaban'][0];?>" required>
          <?php 
            if ((substr($row['Jwaban'][0],0,4)) == ('http')){
              echo "A. <img style='width:155px; height:85px;' src=";echo $row['Jwaban'][0];echo">";
            }else{
              echo " A. "; echo $row['Jwaban'][0];
            }
          ?></td>
      </tr>
      <tr>
        <td class="never"></td>
        <td class="never"></td>
        <td class="never"></td>
        <td><input type="radio" name="data[<?= $no ?>]" value="<?php echo 'B_'; echo $row['Jwaban'][1];?>" required>
           <?php 
            if ((substr($row['Jwaban'][1],0,4)) == ('http')){
              echo "B. <img style='width:155px; height:85px;' src=";echo $row['Jwaban'][1];echo">";
            }else{
              echo " B. "; echo $row['Jwaban'][1];
            }
          ?></td>
      </tr>
      <tr>
        <td></td>
        <td></td>
        <td></td>
        <td><input type="radio" name="data[<?= $no ?>]" value="<?php echo 'C_'; echo $row['Jwaban'][2];?>" required>
           <?php 
            if ((substr($row['Jwaban'][2],0,4)) == ('http')){
              echo "C. <img style='width:155px; height:85px;' src=";echo $row['Jwaban'][2];echo">";
            }else{
              echo " C. "; echo $row['Jwaban'][2];
            }
          ?></td>
      </tr>
      <tr>
        <td></td>
        <td></td>
        <td></td>
        <td><input type="radio" name="data[<?= $no ?>]" value="<?php echo 'D_'; echo $row['Jwaban'][3];?>" required>
           <?php 
            if ((substr($row['Jwaban'][3],0,4)) == ('http')){
              echo "D. <img style='width:155px; height:85px;' src=";echo $row['Jwaban'][3];echo">";
            }else{
              echo " D. "; echo $row['Jwaban'][3];
            }
          ?></td>
      
      <tr>
        <td></td>
        <td></td>
        <td></td>
        <td><input type="radio" name="data[<?= $no ?>]" value="<?php echo 'E_'; echo $row['Jwaban'][4];?>" required>
           <?php 
            if ((substr($row['Jwaban'][4],0,4)) == ('http')){
              echo "E. <img style='width:155px; height:85px;' src=";echo $row['Jwaban'][4];echo">";
            }else{
              echo " E. "; echo $row['Jwaban'][4];
            }
          ?></td>
      </tr>
      <tr>
        <td></td><td></td><td></td><td></td>
      </tr>
<!-- </tr> -->
    <?php endforeach; ?>
    
  </tbody>
</table>
<tr>
      <td colspan="5">
        <div class="pull-right">
              <button type="submit" class="btn btn-warning"><i class="icon-pencil5"></i> Done</button>
        </div>
      </td>
    </tr>
</form>



</div>
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">
<style type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.2/jquery.toast.css"></style>
 <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.2/jquery.toast.js"></script>

<script type="text/javascript">
   $('#example').DataTable({
   "pagingType": "simple_numbers_no_ellipses",
       "searching": false,
       "sort": false,
       "bInfo": false,
       "bLengthChange": false,
       "pageLength": 6
   });

  var table = $('#example').DataTable();
  // TEST
//     var tabless = $('#example_paginate').DataTable();
//     var TEST = table.$("#example_next").attr('id');
//     console.log('dfsdfs',TEST)
//     table.$('#example_next').on('click', 'button', function () {
//   var id = $(this).attr('id'); //$(this) refers the clicked button element
//   console.log(id);
// });
  //
  var btnSave = document.getElementById('quiz');

  // btnSave.addEventListener('submit', function (event) {
  $('#quiz').submit(function(event){
            var NAwal = <?php echo json_encode($Nvalue);?>;
            var hasil = table.$("input:radio:checked").serializeArray()
             hasil.sort(function(a2, b2) {
                  var nameA = a2.name.toUpperCase(); // ignore upper and lowercase
                  var nameB = b2.name.toUpperCase(); // ignore upper and lowercase
                  if (nameA < nameB) {
                    return -1;
                  }
                  if (nameA > nameB) {
                    return 1;
                  }

                  // names must be equal
                  return 0;
          });
          console.log(hasil);
          var All = $('#counts').val();
          console.log(All)
           // console.log(soal)
          if (hasil.length < All){
            var html = '<div class="alert alert-danger"><strong>Isi Data Keseluruhan</strong></div>'
            $('#err').html(html);
             event.preventDefault();
            return false;
          }else{
            $.ajax({
                  url : "<?php echo base_url();?>aksespeserta/SPeserta/add",
                  method : "POST",
                  data : {hasils:hasil, Ns:NAwal},
                  success:function(err,sukses){
                    alert('Selamat Anda Telah Melakukan Ujian')

                  },error:function(err){
                    alert('Terjadi Masalah Sistem')
                  }
                })
                      
          }
    }); 
    
        
       

// });

</script>

<script type="text/javascript">
window.onbeforeunload = function(event) {
        var table = $('#example').DataTable();
            var hasil = table.$("input:radio:checked").serializeArray()
             hasil.sort(function(a2, b2) {
                  var nameA = a2.name.toUpperCase(); // ignore upper and lowercase
                  var nameB = b2.name.toUpperCase(); // ignore upper and lowercase
                  if (nameA < nameB) {
                    return -1;
                  }
                  if (nameA > nameB) {
                    return 1;
                  }
                  return 0;
            });
              var NAwal = <?php echo json_encode($Nvalue);?>;
          console.log(hasil.length)
           console.log(hasil)
          // if (hasil.length === 0){
          //   if (hasil.length < All){
          //   var html = '<div class="alert alert-danger"><strong>Isi Data Keseluruhan</strong></div>'
          //   $('#err').html(html);
          //    event.preventDefault();
          //   return false;
          // }else{
              // var All = $('#counts').val();
             
            $.ajax({
                  url : "<?php echo base_url();?>aksespeserta/SPeserta/add",
                  method : "POST",
                  data : {hasils:hasil, Ns:NAwal},
                  success:function(err,sukses){
                    alert('Selamat Anda Telah Melakukan Ujian')


                  },error:function(err){
                    alert('Terjadi Masalah Sistem')
                  }
                })
                      
          // }
    
          // }else{
          //       $.ajax({
          //         url : "<?php echo base_url();?>aksespeserta/SPeserta/add",
          //         method : "POST",
          //         data : {hasils:hasil, Ns:NAwal},
          //         success:function(err,sukses){
          //           alert('Selamat Anda Telah Melakukan Ujian')

          //         },error:function(err){
          //           alert('Terjadi Masalah Sistem')
          //         }
          //       })
    
          // }
    
}



</script>
