<?php 
$data=$this->session->flashdata('sukses');
if($data!=""){ ?>
<div class="alert alert-success"><strong>Sukses! </strong> <?=$data;?></div>
<?php } ?>
<?php 
$data2=$this->session->flashdata('error');
if($data2!=""){ ?>
<div class="alert alert-danger"><strong> Error! </strong> <?=$data2;?></div>
<?php } ?>

<div class="panel panel-primary">
  <div class="panel-heading" style="    color: #fff;
    background-color: #ff5722;
    border-color: #ff5722;">
    <h5 class="panel-title"><i class="icon-people"></i>Edit Data Peserta</h5>
  </div>

    <div class="panel-body">
      <div class="well well-sm">
      
        <form action="<?php echo site_url('Peserta/edit/'.$hasil['id_peserta']); ?>" method="post">
          <div class="row">
            <!-- <div class="col-md-4">
               <span class="input-group-addon"><i class="fa fa-user"> NO INDUK</i></span>
               <input type="number" name="no_induk" id="no_induk" value="<?php echo $hasil['no_induk'];?>" placeholder="Nomor Induk" class="form-control" required>
            
            </div> -->
      
      <div class="col-md-4">
        
          <span class="input-group-addon"><i class="fa fa-user"> Nama</i></span>
            <input type="text" name="nama" autocomplete="off" value="<?php echo $hasil['nama'];?>" placeholder="Nama" class="form-control" required>
         
      </div> 

        <div class="col-md-4">
          <span class="input-group-addon"><i class="fa fa-user"> Jenis Kelamin</i></span>
         <select name="jns_kelamin" class="select-clear" required>
          <option value="">Jenis Kelamin</option>
         <option <?php if ($hasil['jenis_kelamin'] == 'Laki-laki'){echo "selected";} ?> value="Laki-Laki">Laki-Laki</option>
        <option <?php if ($hasil['jenis_kelamin'] == 'Perempuan'){echo "selected";} ?> value="Perempuan">Perempuan</option>
                  
         </select>
        </div> 
        <div class="col-md-4">
        
          <span class="input-group-addon"><i class="fa fa-envelope"> Email</i></span>
            <input type="email" name="email" autocomplete="off" value="<?php echo $hasil['email'];?>" placeholder="Email" class="form-control" required>
         
      </div> 
      </div> 
    <br>
    <div class="row">
      <div class="col-md-4">
        <center>
          <span class="input-group-addon"><i class="fa fa-calendar"> Tanggal Lahir</i></span>
             <input type="date" name="tgl_lahir" value="<?php echo $hasil['tgl_lahir'];?>" autocomplete="off" placeholder="Tanggal Lahir" class="form-control" required>
          
        </center>
      </div>
    
      <div class="col-md-4">
        <span class="input-group-addon"><i class="fa fa-phone"> Hp</i></span>
          <input type="number" name="hp" id="hp" value="<?php echo $hasil['hp'];?>" placeholder="Hp" class="form-control" required>
     
    </div>
    
    <div class="col-md-4">
      <span class="input-group-addon"><i class="fa fa-road"> Alamat</i></span>
        <textarea name="alamat" id="alamat" placeholder="Alamat" class="form-control" required><?php echo $hasil['alamat'];?></textarea>
      </div>
    
    </div>
    <div class="row"> 
    <div class="col-md-4">
      <button type="submit" class="btn btn-warning btn-sm"><i class="icon-file-plus"></i> Edit Data Anggota </button>
    </div>
    </div>
    </form>
    <br>
    <form action="<?php echo site_url('Anggota/editAcc/'.$hasil['id_peserta']); ?>" method="post">
    <div class="row">
      <div class="col-md-4">
        <center>
          <span class="input-group-addon"><i class="fa fa-user"> Username</i></span>
             <input type="text" name="username" value="<?php echo $hasil['username'];?>" autocomplete="off" placeholder="username" class="form-control" required>
          
        </center>
      </div>
    
      <div class="col-md-4">
        <span class="input-group-addon"><i class="fa fa-code"> New Password</i></span>
          <input type="password" name="password" id="password" placeholder="password" class="form-control" required>
     
    </div>
     <div class="row"> 
    <div class="col-md-4">
      <button type="submit" class="btn btn-warning btn-sm"><i class="icon-user"></i> Edit Data Account Anggota </button>
    </div>
    </div>
   
    </div>
    </form>

    
  </div>
  
  <br>
    </div>
  </div>
