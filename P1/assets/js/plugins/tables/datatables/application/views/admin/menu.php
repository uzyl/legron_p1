<div class="sidebar sidebar-main" style="">
  <div class="sidebar-content">
    <div class="sidebar-user">
    </div>
    <div class="sidebar-category sidebar-category-visible">
      <div class="category-content no-padding">
        <ul class="navigation navigation-main navigation-accordion">
          
          <li class="<?php echo menuaktif('dashboard',$aktif); ?>"><a href="<?php echo base_url(); ?>"><i class="icon-home4"></i> <span>Dashboard</span></a></li>
          <li>
            <a href="#"><i class="icon-stack2"></i> <span>Master Data</span></a>
            <ul>
              <li class="<?php echo menuaktif('soal',$aktif); ?>"><a href="<?php echo site_url('Soal'); ?>"><i class="icon-sphere"></i>Soal</a></li>
           <!--    <li class="<?php echo menuaktif('kategorisoal',$aktif); ?>"><a href="<?php echo site_url('KategoriSoal'); ?>"><i class="icon-pencil6"></i>Kategori Soal</a></li>
              <li class="<?php echo menuaktif('kategoripek',$aktif); ?>"><a href="<?php echo site_url('KatSoalPek'); ?>"><i class="fa fa-briefcase"></i>Kategori Soal Pekerjaan</a></li>
             --></ul>
          </li>
          <!-- <li class="<?php echo menuaktif('seleksi',$aktif); ?>"><a href="<?php echo site_url('Seleksi'); ?>"><i class="icon-people"></i> <span>Seleksi Calon Pegawai</span></a></li> -->
          <!-- <li class="<?php echo menuaktif('hasilujian',$aktif); ?>"><a href="<?php echo site_url('HasilUjian'); ?>"><i class="icon-users"></i> <span>Hasil Jawaban Peserta</span></a></li> -->
          <li class="<?php echo menuaktif('hasil',$aktif); ?>"><a href="<?php echo site_url('Hasil'); ?>"><i class="icon-users"></i> <span>Hasil Test</span></a></li>
          <li class="<?php echo menuaktif('admin',$aktif); ?>"><a href="<?php echo site_url('Admin'); ?>"><i class="icon-collaboration"></i> <span>Manajemen Akses Admin</span></a></li>
          <li class="<?php echo menuaktif('peserta',$aktif); ?>"><a href="<?php echo site_url('Peserta'); ?>"><i class="icon-people"></i> <span>Data Peserta</span></a></li>
          
        </ul>
      </div>
    </div>
  </div>
</div>