<?php

class Hasil extends CI_Controller {
	function __construct(){

		parent::__construct();
				$this->load->model('M_soal');
				$this->load->model('M_jawaban');
		$this->load->model('M_penilaian');
		$this->load->helper('url');
		$config['tag_open'] = '<ul class="breadcrumb">';
		$config['tag_close'] = '</ul>';
		$config['li_open'] = '<li>';
		$config['li_close'] = '</li>';
		$config['divider'] = '<span class="divider"> » </span>';
		$this->breadcrumb->initialize($config);
		no_access();
		levelsuper();
	}

	public function index()
	{
		$data=array(
			"title"=>'Hasil Keseluruhan Ujian',
			"menu"=>getmenu(),
			"aktif"=>"hasil",
			"all"=>$this->M_penilaian->getAll(),
			// "allTbl" => $this->M_penilaian->getForTbl(),
			"content"=>"hasil/index.php",
		);
		$this->breadcrumb->append_crumb('Hasil Data Peserta Ujian', site_url('Hasil'));
		$this->load->view('admin/template',$data);
	}	

	public function hasilpeserta($id)
	{
		$ALL = $this->M_soal->getSoal();
		$ALLOrder = $this->M_soal->getSoal1();
		$ALLOrderDeskripsi = $this->M_soal->getSoalDeskrip();
		$HasilJwb = $this->M_jawaban->getHasil($id);
		
		$arraySoal = array();
		$arrayJawabanA = array();
		$arrayJawabanB = array();
		$arrayJawabanC = array();
		$arrayJawabanD = array();
		$arrayJawabanE = array();
			foreach ($ALL as $key => $result) {

						if (substr($result->kat_soal,0,1) == "A"){
							$arrayJawabanA[] = $result->jawaban;
						}
						elseif (substr($result->kat_soal,0,1) == "B"){
							$arrayJawabanB[] = $result->jawaban;
						}
						elseif (substr($result->kat_soal,0,1) == "C"){
							$arrayJawabanC[] = $result->jawaban;
						}
						elseif (substr($result->kat_soal,0,1) == "D"){
							$arrayJawabanD[] = $result->jawaban;
						}
						elseif (substr($result->kat_soal,0,1) == "E"){
							$arrayJawabanE[] = $result->jawaban;
						}
				
			}

			foreach ($ALLOrder as $key1 => $result1) {
					$arraySoal[$key1]["soal"] = $result1->soal;
					$arraySoal[$key1]["deskripsi"] =$result1->deskripsi;
					$arraySoal[$key1]["Jwaban"] = [
						$arrayJawabanA[$key1],
						$arrayJawabanB[$key1],
						$arrayJawabanC[$key1],
						$arrayJawabanD[$key1],
						$arrayJawabanE[$key1]
					];
			}
		$data=array(
			"title"=>'Hasil Ujian Peserta',
			"menu"=>getmenu(),
			"aktif"=>"hasil",
			"all"=>$arraySoal,
			"hasill"=>$HasilJwb,
			// "allTbl" => $this->M_penilaian->getForTbl(),
			"content"=>"hasil/hasilpeserta.php",
		);
		$this->breadcrumb->append_crumb('Hasil Data Peserta Ujian', site_url('Hasil/ujian-peserta/'.$id));
		$this->load->view('admin/template',$data);
	}	


	public function hapus($id)
	{
		$hasil = $this->db->query("DELETE FROM result where id_peserta='$id'");
		if ($hasil == true){
			$this->db->query("DELETE FROM jawaban where id_peserta='$id'");
			$this->session->set_flashdata('sukses', 'Data Berhasil Di Hapus');
			redirect('Hasil');
		}else{
			$this->session->set_flashdata('error', 'Data Gagal Di Hapus');
			redirect('Hasil');
		}
		
	}

	public function edit($id)
	{
		$data=array(
				// "nama"=>$_POST['nama'],
				"nilai"=>$_POST['nilai'],
				"predikat"=>$_POST['predikat']
		);
		$this->M_penilaian->edit($id,$data);
		$this->session->set_flashdata('sukses', 'Data Berhasil Di Edit');
		redirect('Hasil');
	}

	
}
