<?php

class Seleksi extends CI_Controller {
	function __construct(){

		parent::__construct();
		$this->load->model('M_hasil');
		$this->load->model('M_klasifikasi');
		$this->load->helper('url');
		$config['tag_open'] = '<ul class="breadcrumb">';
		$config['tag_close'] = '</ul>';
		$config['li_open'] = '<li>';
		$config['li_close'] = '</li>';
		$config['divider'] = '<span class="divider"> » </span>';
		$this->breadcrumb->initialize($config);
		no_access();
		levelsuper();
	}

	public function index()
	{
		$data=array(
			"title"=>'Seleksi Calon Pegawai',
			"menu"=>getmenu(),
			"aktif"=>"seleksi",
			"opt"=>$this->M_klasifikasi->getPeserta(),
			"all"=>$this->M_hasil->getAll(),
			"content"=>"seleksi/index.php",
		);
		// $dataDouble = $this->M_klasifikasi->DoubleGet();
		// print_r($dataDouble);
		$this->breadcrumb->append_crumb('Seleksi Calon Pegawai', site_url('Seleksi'));
		$this->load->view('admin/template',$data);
	}	

	public function getSubPeserta()
	{
		$nama=$this->input->post('namas');
		$opt=$this->M_hasil->getJoin($nama);
		echo json_encode($opt);
		// print_r($opt);
		
	}	

	public function getSubHasil()
	{
		$nama=$this->input->post('namas');
		$id_kat_pkj=$this->input->post('id_katPkj');
		// print_r($nama);
		$dataDouble = $this->M_klasifikasi->DoubleGet($nama, $id_kat_pkj);
		echo json_encode($dataDouble);
	}	

	public function fuzzy()
	{
		$nama = $this->input->post('nama');
		$jk = $this->input->post('jk');
		$tgl_lahir = $this->input->post('tgl_lahir');
		$hp = $this->input->post('hp');
		$email = $this->input->post('email');
		$alamat = $this->input->post('alamat');
		$pendidikan = $this->input->post('pendidikan');
		$posisi = $this->input->post('posisi');

		$wawancara = $this->input->post('wawancara');
		$administrasi = $this->input->post('administrasi');
		$kepribadian = $this->input->post('kepribadian');
		$angkaHasil = (int)$wawancara + (int)$administrasi + (int)$kepribadian;
		$Results = $angkaHasil/3; 
			if ($Results >= 65){
				$data=array(
					"nama"=>$nama,
					"jenis_kelamin"=>$jk,
					"tgl_lahir"=>$tgl_lahir,
					"hp"=>$hp,
					"email"=>$email,
					"alamat"=>$alamat,
					"pendidikan"=>$pendidikan,
					"posisi"=>$posisi,
					"kepribadian"=>$kepribadian,
					"wawancara"=>$wawancara,
					"administrasi"=>$administrasi,
					"hasil"=>$Results,
					"kelayakan"=>'Layak',
					"date_created" => date('d M Y H:i')
				);
				$results = $this->M_hasil->postData($data);
				if ($results == true){
					$this->session->set_flashdata('sukses', 'Data Layak Sebagai Calon');
					redirect('Seleksi');	
				}else {
					$this->session->set_flashdata('sukses', 'Gagal Memverifikasi');
					redirect('Seleksi');	
				}
			}else {
				$data=array(
					"nama"=>$nama,
					"jenis_kelamin"=>$jk,
					"tgl_lahir"=>$tgl_lahir,
					"hp"=>$hp,
					"email"=>$email,
					"alamat"=>$alamat,
					"pendidikan"=>$pendidikan,
					"posisi"=>$posisi,
					"kepribadian"=>$kepribadian,
					"wawancara"=>$wawancara,
					"administrasi"=>$administrasi,
					"hasil"=>$Results,
					"kelayakan"=>'Tidak Layak',
					"date_created" => date('d M Y H:i')
				);
				$results = $this->M_hasil->postData($data);
				if ($results == true){
					$this->session->set_flashdata('sukses', 'Data Tidak Layak Sebagai Calon');
					redirect('Seleksi');	
				}else {
					$this->session->set_flashdata('sukses', 'Gagal Memverifikasi');
					redirect('Seleksi');	
				}
			}
	}

	
}
