<?php

class SPeserta extends CI_Controller {
	function __construct(){

		parent::__construct();

		$this->load->model('M_penilaian');
		$this->load->model('M_peserta');
		$this->load->model('M_soal');
		$this->load->model('M_jawaban');
		$this->load->helper('url');
		// $this->load->helper('html');
		$config['tag_open'] = '<ul class="breadcrumb">';
		$config['tag_close'] = '</ul>';
		$config['li_open'] = '<li>';
		$config['li_close'] = '</li>';
		$config['divider'] = '<span class="divider"> » </span>';
		$this->breadcrumb->initialize($config);
		no_access();
		levelpesert();
		udahIsi();
	}
	public function index()
	{
		$ALL = $this->M_soal->getSoal();
		$ALLOrder = $this->M_soal->getSoal1();
		$ALLOrderDeskripsi = $this->M_soal->getSoalDeskrip();
		
		$arraySoal = array();
		$arrayJawabanA = array();
		$arrayJawabanB = array();
		$arrayJawabanC = array();
		$arrayJawabanD = array();
		$arrayJawabanE = array();
			foreach ($ALL as $key => $result) {
						//$resul = data obyek dari sebuah atribut
						//$key = array jumlah soal sebanyak 60 soal
						if (substr($result->kat_soal,0,1) == "A"){
							$arrayJawabanA[] = $result->jawaban;
						}
						elseif (substr($result->kat_soal,0,1) == "B"){
							$arrayJawabanB[] = $result->jawaban;
						}
						elseif (substr($result->kat_soal,0,1) == "C"){
							$arrayJawabanC[] = $result->jawaban;
						}
						elseif (substr($result->kat_soal,0,1) == "D"){
							$arrayJawabanD[] = $result->jawaban;
						}
						elseif (substr($result->kat_soal,0,1) == "E"){
							$arrayJawabanE[] = $result->jawaban;
						}
				
			}

			foreach ($ALLOrder as $key1 => $result1) {
					$arraySoal[$key1]["soal"] = $result1->soal;
					$arraySoal[$key1]["deskripsi"] =$result1->deskripsi;
					$arraySoal[$key1]["Jwaban"] = [
						$arrayJawabanA[$key1],
						$arrayJawabanB[$key1],
						$arrayJawabanC[$key1],
						$arrayJawabanD[$key1],
						$arrayJawabanE[$key1]
					];
			}
			// echo json_encode($arraySoal);
			// echo json_encode($ALLOrder);
			// echo json_encode($ALLOrderDeskripsi);
			// echo json_encode(count($arraySoal));
			// echo json_encode($this->session->userdata('identity'));
			
				$JumlahSoal = count($arraySoal); // AS MOD
				$IdentitasPeserta = $this->session->userdata('identity'); // AS X0



				/// MANUAL
				// $a = 1;
				// $Mod = 60;
				$arrayResult = array();
				$arrayN = array();
				for($i=0; $i<$JumlahSoal;$i++){
					
					$IdentitasPeserta = ((1 * $IdentitasPeserta) + 7)%$JumlahSoal;
						$arrayResult[$i]  = $arraySoal[$IdentitasPeserta];
						$arrayN[$i]=$IdentitasPeserta;
						
				}

			$data=array(
				"title"=>'Soal',
				"menu"=>getmenu(),
				"all"=>$arrayResult,
				"Nvalue"=>$arrayN,
				"aktif"=>"soal",
				"content"=>"soal/soal.php",
			);

			$this->breadcrumb->append_crumb('Soal', site_url('aksespeserta/soal'));
			$this->load->view('peserta/template',$data);
	}

	public function add()
	{	

		$username=$this->session->userdata('user');
        $qu=$this->M_peserta->getOneUsername($username);
        $ALLOrder = $this->M_soal->getSoal1();
        if ($qu == true){
			$hasil = $this->input->post();
			$result = array();
				foreach ($hasil['hasils'] as $nilaiArray => $value) {
						$result[$hasil['Ns'][$nilaiArray]]['hasil'] = $value['value'];	
						
				}

				$nilai = array();
				foreach ($ALLOrder as $key => $value) {
					// if ($key == $result[$key]['N']){
						$data = array(
	        			"nama_peserta" => $qu["nama"],
	         			"id_peserta" => $qu["id_peserta"],
	         			"soal" =>$value->soal,
	         			"jawaban" =>$result[$key]['hasil'],
	         			"date_created" => date("Y-m-d H:i:s")
		        		);
		        		
		        		// if ($value == substr($result[$key]['hasil'])){

		        		// }

		        		$nilai[$key] = $result[$key]['hasil'];
		        		$this->M_jawaban->postJwb($data);
	        	}

	        	$ArrhasilAkhir = array();
	        	foreach ($ALLOrder as $key => $value) {
					if ($value->setjawaban == substr($result[$key]['hasil'],0,1)){
						$ArrhasilAkhir[$key] =  $result[$key]['hasil'];
					}
					
	        	}

	        		$RESULT = ((count($ArrhasilAkhir) * 10) / count($ALLOrder));
						$predikat ='';
						if ($RESULT <= 3.5){
							$predikat = 'Kurang';
						}elseif (($RESULT >=  3.6) && ($RESULT <= 6.5)) {
							$predikat = 'Cukup';
						}elseif (($RESULT >=  6.6) && ($RESULT <= 8.0)) {
							$predikat = 'Baik';
						}elseif (($RESULT >=  8.1) && ($RESULT <= 10)) {
							$predikat = 'Sangat Baik';
						}
						$data = array(
							"id_peserta" => $qu["id_peserta"],
	         				"nilai" => $RESULT,
	         				"predikat" => $predikat,
	         				"date_created" => date("Y-m-d H:i:s")
						);
						$Done = $this->M_penilaian->postJwb($data);
						if ($Done == true){
							$this->session->set_flashdata('Sukses',"Anda Berhasil Test");
       						redirect('aksespeserta/Home/');
						}else{
							$this->session->set_flashdata('error',"Terjadi Kesalahan");
       						redirect('aksespeserta/Home/');
						}

       	}else{
			$this->session->set_flashdata('error',"Terjadi Kesalahan");
       		redirect('aksespeserta/Home/');
       	}
	}

}
