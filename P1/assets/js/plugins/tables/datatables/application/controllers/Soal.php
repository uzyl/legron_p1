<?php

class Soal extends CI_Controller {
	function __construct(){

		parent::__construct();
		$this->load->model('M_soal');
		$this->load->helper('url');
		$config['tag_open'] = '<ul class="breadcrumb">';
		$config['tag_close'] = '</ul>';
		$config['li_open'] = '<li>';
		$config['li_close'] = '</li>';
		$config['divider'] = '<span class="divider"> » </span>';
		$this->breadcrumb->initialize($config);
		no_access();
		levelsuper();
	}
	public function index()
	{
		$data=array(
			"title"=>'Soal',
			"menu"=>getmenu(),
			"all"=>$this->M_soal->getSoalAdm(),
			// "katSoal"=>$this->M_soal->getKatCombx(),
			// "katGr"=>$this->M_soal->getKatgr(),
			"aktif"=>"soal",
			"content"=>"soal/index.php",
		);
		$this->breadcrumb->append_crumb('Soal', site_url('soal'));
		$this->load->view('admin/template',$data);
	}

	public function add()
	{
		// print_r($this->input->post());
		$hasil = $this->input->post();
		echo print_r($hasil);
		foreach($hasil['has'] as $d){
			foreach ($d['jawaban'] as $key) {
				
       			$data=array(
							"kat_soal"=>$key['name'],
							"setjawaban"=>$d['SoalsJwb'],
							"soal"=>$d['soal'],
							"deskripsi"=>$hasil['desk'],
							"jawaban"=>$key['value'],
							"date_created" => date("Y-m-d H:i:s")
						);
        				$result  = $this->M_soal->postSoal($data);
        	}
    	}
    		$this->session->set_flashdata('sukses',"Soal Sukses Di Simpan");
			// redirect('Soal');
		
	}


	public function tambah()
	{	
		// $data['set'] = $_POST['set'];
		$data=array(
			"sets"=>$this->input->post('set'),
			"title"=>'Soal',
			"menu"=>getmenu(),
			"all"=>$this->M_soal->getSoal(),
			// "katSoal"=>$this->M_soal->getKatCombx(),
			// "katGr"=>$this->M_soal->getKatgr(),
			"aktif"=>"soal",
			"content"=>"soal/tambah.php",
		);
		$this->breadcrumb->append_crumb('Tambah Soal', site_url('soal/tambah'));
		$this->load->view('admin/template',$data);
	}

	public function edit($id){
		// $nama = $_POST['soal'];
		$this->form_validation->set_rules('jawaban', 'jawaban', 'required');
		$this->form_validation->set_rules('soal', 'soal', 'required');
		// $this->form_validation->set_rules('deskripsi', 'deskripsi', 'required');
		if($this->form_validation->run()==FALSE){
			$this->session->set_flashdata('error',"Data Anda Gagal Di Edit");
			redirect('Soal');
		}else{
			// $soal = $this->input->post('ceksoal');
			// $deskripsi = $_POST['cekdeskripsi'];
			$jawaban=array(
				// "soal"=>$_POST['soal'],
				"jawaban"=>$_POST['jawaban'],
			);
			$dataSoal=array(
				"soal"=>$_POST['soal'],
				"setjawaban"=>$_POST['setjwb']
			);
			// $dataSetJwb=$_POST['setjwb'];
			$dataDeskripsi=array(
				"deskripsi"=>$_POST['deskripsi'],
			);
			
			$this->M_soal->editSoal($dataSoal,$id,$jawaban,$dataDeskripsi);
			$this->session->set_flashdata('sukses',"Data Berhasil Diedit");
			redirect('Soal');
		}
		// print_r($id);
	}

	public function hapus($soal)
	{
		if($soal==""){
			$this->session->set_flashdata('error',"Data Anda Gagal Di Hapus");
			redirect('Soal');
		}else{
			$this->M_soal->deleteSoal($soal);
			$this->session->set_flashdata('sukses',"Data Berhasil Dihapus");
			redirect('Soal');
		}
		// print_r($id);
	}
}
