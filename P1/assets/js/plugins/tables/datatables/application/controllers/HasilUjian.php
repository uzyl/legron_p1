<?php

class HasilUjian extends CI_Controller {
	function __construct(){

		parent::__construct();
		// $this->load->model('M_hasil');
		$this->load->model('M_penilaian');
		$this->load->helper('url');
		$config['tag_open'] = '<ul class="breadcrumb">';
		$config['tag_close'] = '</ul>';
		$config['li_open'] = '<li>';
		$config['li_close'] = '</li>';
		$config['divider'] = '<span class="divider"> » </span>';
		$this->breadcrumb->initialize($config);
		no_access();
		levelsuper();
	}

	public function index()
	{
		$data=array(
			"title"=>'Hasil Keseluruhan Jawaban Peserta',
			"menu"=>getmenu(),
			"aktif"=>"hasilujian",
			"all"=>$this->M_penilaian->getAll(),
			// "allTbl" => $this->M_penilaian->getForTbl(),
			"content"=>"hasiljawaban/index.php",
		);
		$this->breadcrumb->append_crumb('Hasil Data Peserta Jawaban Peserta', site_url('HasilUjian'));
		$this->load->view('admin/template',$data);
	}	



	public function hapus($id)
	{
		$this->db->query("DELETE FROM result where id='$id'");
		$this->session->set_flashdata('sukses', 'Data Berhasil Di Hapus');
		redirect('Hasil');
	}

	public function edit($id)
	{
		$data=array(
				// "nama"=>$_POST['nama'],
				"nilai"=>$_POST['nilai'],
				"predikat"=>$_POST['predikat']
		);
		$this->M_penilaian->edit($id,$data);
		$this->session->set_flashdata('sukses', 'Data Berhasil Di Edit');
		redirect('Hasil');
	}

	
}
