<?php

class Hasil extends CI_Controller {
	function __construct(){

		parent::__construct();
		$this->load->model('M_penilaian');
		$this->load->model('M_peserta');
		$this->load->helper('url');
		$config['tag_open'] = '<ul class="breadcrumb">';
		$config['tag_close'] = '</ul>';
		$config['li_open'] = '<li>';
		$config['li_close'] = '</li>';
		$config['divider'] = '<span class="divider"> » </span>';
		$this->breadcrumb->initialize($config);
		no_access();
		levelpesert();
	}

	public function index()
	{
		$id = $this->session->userdata('id_peserta');
        $bio=$this->M_peserta->getOneIds($id);
        $hasils = $this->M_penilaian->getOneId($id);
		$data=array(
			"title"=>'Hasil Ujian',
			"menu"=>getmenu(),
			"aktif"=>"hasiltest",
			"hasil"=>$hasils,
			"bio"=>$bio,
			"content"=>"hasil/index.php",
		);
		$this->breadcrumb->append_crumb('Hasil Ujian', site_url('aksespeserta/Hasil'));
		$this->load->view('peserta/template',$data);
	}	

	public function getSubPeserta()
	{
		$nama=$this->input->post('namas');
		$opt=$this->M_hasil->getJoin($nama);
		echo json_encode($opt);
		// print_r($opt);
		
	}	

	public function getSubHasil()
	{
		$nama=$this->input->post('namas');
		$id_kat_pkj=$this->input->post('id_katPkj');
		// print_r($nama);
		$dataDouble = $this->M_klasifikasi->DoubleGet($nama, $id_kat_pkj);
		echo json_encode($dataDouble);
	}	

	public function getJawaban(){
		$nama=$this->input->post('nama');
		$data = $this->M_klasifikasi->getOneName($nama);
		echo json_encode($data);
	}

}
