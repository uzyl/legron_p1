<?php

class Admin extends CI_Controller {
	function __construct(){

		parent::__construct();
		$this->load->model('M_admin');
		$this->load->helper('url');
		$config['tag_open'] = '<ul class="breadcrumb">';
		$config['tag_close'] = '</ul>';
		$config['li_open'] = '<li>';
		$config['li_close'] = '</li>';
		$config['divider'] = '<span class="divider"> » </span>';
		$this->breadcrumb->initialize($config);
		no_access();
		levelsuper();
	}
	public function index()
	{
		$data=array(
			"title"=>'Manajemen Akses',
			"menu"=>getmenu(),
			"all"=>$this->M_admin->getAdmin(),
			"aktif"=>"admin",
			"content"=>"admin/index.php",
		);
		$this->breadcrumb->append_crumb('Admin', site_url('admin'));
		$this->load->view('admin/template',$data);
	}
	public function add()
	{
		
			$data=array(
				"id_admin"=>"",
				"nama"=>$_POST['nama'],
				"username"=>$_POST['username'],
				"password"=>md5($_POST['password']),
				"akses"=>$_POST['akses'],
				"status"=>1
			);
			$this->M_admin->postAdmin($data);
			$this->session->set_flashdata('sukses', 'Data Berhasil Di Tambahkan');
			redirect('Admin');
		
		
	}
	public function edit($id)
	{
		$data=array(
			"nama"=>$_POST['nama'],
			"username"=>$_POST['username'],
			"password"=>md5($_POST['password']),
			"akses"=>$_POST['akses']
		);
		$this->M_admin->editAdmin($id,$data);
		$this->session->set_flashdata('sukses', 'Data Berhasil Di Update');
		redirect('Admin');
	}
	public function delete($id)
	{
		$this->db->query("DELETE FROM admin where id_admin='$id'");
		$this->session->set_flashdata('sukses', 'Data Berhasil Di Hapus');
		redirect('Admin');
	}
}
