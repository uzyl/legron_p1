<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_soal extends CI_Model {
	var $tabel    = 'soal';
	function __construct()
	{
		parent::__construct();
	}
	
	function getSoalAdm()
	{
	return	$this->db->query("select * from soal order by date_created")->result();
	}

	function getSoal()
	{
	return	$this->db->query("select * from soal order by date_created")->result();
	}

	function getSoal1()
	{
	return	$this->db->query("SELECT * FROM soal GROUP BY soal ORDER BY date_created")->result();
	}

	function getSoalDeskrip()
	{
	return	$this->db->query("SELECT * FROM soal GROUP BY deskripsi")->result();
	}

	function getKatCombx()
	{
	return	$this->db->query("select * from kat_soal where not exists (select id_kat from soal where soal.id_kat = kat_soal.id_kat_soal)")->result();
	}

	function getKatgr()
	{
	return	$this->db->query("select * from kat_soal")->result();
	}

	function postSoal($data)
	{
	return $this->db->insert($this->tabel, $data);
	}

	function editSoal($dataSoal,$id,$jawaban,$dataDeskripsi){
		$hasil = $this->db->query("select * from soal where id_soal='$id'")->row_array();
		$this->db->where('id_soal', $id);
		$this->db->update('soal',$jawaban);
		$this->db->where('soal',$hasil['soal']);
		$this->db->update('soal',$dataSoal);
		$this->db->where('deskripsi',$hasil['deskripsi']);
		$this->db->update('soal',$dataDeskripsi);
	}

	function deleteSoal($soal){
		$this->db->where('soal', $soal);
			$this->db->delete('Soal');
	}


	function getKatKerjaOne($posisi){
		return $this->db->query("select kat_soal_pekerjaan.*, kat_soal.* from kat_soal_pekerjaan 
				inner join kat_soal on kat_soal_pekerjaan.id_kat_pkj = kat_soal.id_kat_soal where 
				kat_soal_pekerjaan.pekerjaan = '$posisi'")->row_array();
	}

}

/* End of file M_entry.php */
/* Location: ./application/models/M_entry.php */