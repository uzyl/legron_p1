<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_penilaian extends CI_Model {
	var $tabel    = 'result';
	function __construct()
	{
		parent::__construct();
	}


	function postJwb($data)
	{
	return $this->db->insert($this->tabel, $data);
	}

	function getOneId($id){
		return $this->db->query("select * from result where id_peserta='$id'")->row_array();
	}

	function getAll(){
			return	$this->db->query("
SELECT result.*, peserta.nama, peserta.jenis_kelamin, peserta.hp, peserta.tgl_lahir FROM result INNER JOIN peserta ON peserta.id_peserta = result.id_peserta ORDER BY peserta.id_peserta")->result();
	}

	function edit($id,$data){
		$this->db->where('id', $id);
		$this->db->update('result', $data);
	}


}

/* End of file M_entry.php */
/* Location: ./application/models/M_entry.php */