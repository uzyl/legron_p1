<?php 
function in_access()
{
    $ci=& get_instance();
    if($ci->session->userdata('level')==1){
       redirect('Dash');
    }else if($ci->session->userdata('level')==2){
        redirect('Dash');
    }else if($ci->session->userdata('level')==3){
        redirect('aksespeserta/Home');
    }
}
function no_access()
{
    $ci=& get_instance();
    if(!$ci->session->userdata('user')){
        redirect('login');
    }
}
function menuaktif($aktif,$menu){	
	if($aktif==$menu){
		return "active";
	}else{
		return "";
	}
}
function getmenu(){	
    $CI =& get_instance();
	if($CI->session->userdata('level')==1){
        return "menu.php";
    }else if($CI->session->userdata('level')==2){
        return "menuadmin.php";
    }else if($CI->session->userdata('level')==3){
        return "menuuser.php";
    }else{
        redirect('login');   
    }
}

function udahIsi(){
     $CI =& get_instance();
     $idpeserta = $CI->session->userdata('id_peserta');
        $q = $CI->db->query("select * from result where id_peserta='$idpeserta'")->num_rows();
        // print_r($q['nama']);
        if ($q == true){
            $CI->session->set_flashdata("error","Anda Sudah Melakukan Ujian");
            redirect('aksespeserta/Home');
        }
    // if($CI->session->userdata('user')==$q['nama']){
        // redirect('peserta/Home');

    
}

function levelsuper(){ 
    $CI =& get_instance();
    if($CI->session->userdata('level')!=1){
        $CI->session->set_flashdata('error', "Anda Tidak Memiliki Akses Pada Halaman Tersebut");
        redirect('aksespeserta/Home');
    }
}

function levelpesert(){
    $CI =& get_instance();
    if($CI->session->userdata('level')!=3){
        $CI->session->set_flashdata('error', "Anda Tidak Memiliki Akses Pada Halaman Tersebut");
        redirect('Dash');
    }
}

function getId($tabel,$id)
{
	$ci=& get_instance();
    $q = $ci->db->query("select MAX(".$id.") as kd_max from ".$tabel."");
    $kd = "";
    if($q->num_rows()>0)
    {
        foreach($q->result() as $k)
        {
            $tmp = ((int)$k->kd_max)+1;
            $kd = sprintf("%01s", $tmp);
        }
    }
    else
    {
        $kd = "1";
    }
    return $kd;
}

function getnama($id)
{
    $ci=& get_instance();
    $q = $ci->db->query("select * from admin where id_admin='$id'")->row_array();
    return $q['nama'];
}
